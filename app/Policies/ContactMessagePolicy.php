<?php

namespace App\Policies;

use App\ContactMessage;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContactMessagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(ContactMessage $contactmessage)
    {
        return $contactmessage->id == $contactmessage->id ;
    }
}
