<?php

namespace App\Policies;

use App\SeoReferance;
use Illuminate\Auth\Access\HandlesAuthorization;

class SeoReferancePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(SeoReferance $seoreferance)
    {
        return $seoreferance->id;
    }
}
