<?php

namespace App\Policies;

use App\Cutstomer;
use Illuminate\Auth\Access\HandlesAuthorization;

class CutstomerPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(User $user, Cutstomer $cutstomers)
    {
        return $cutstomers->id;
    }
}
