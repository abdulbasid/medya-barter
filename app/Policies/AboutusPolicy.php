<?php

namespace App\Policies;

use App\Aboutus;
use Illuminate\Auth\Access\HandlesAuthorization;

class AboutusPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(User $user, Aboutus $aboutus)
    {
        return $aboutus->id;
    }
}
