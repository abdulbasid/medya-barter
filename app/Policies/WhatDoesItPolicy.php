<?php

namespace App\Policies;

use App\WhatDoesIt;
use Illuminate\Auth\Access\HandlesAuthorization;

class WhatDoesItPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(WhatDoesIt $whatDoesIt)
    {
        return $whatDoesIt->id;
    }
}
