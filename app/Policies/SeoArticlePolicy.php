<?php

namespace App\Policies;

use App\SeoArticle;
use Illuminate\Auth\Access\HandlesAuthorization;

class SeoArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(SeoArticle $seoarticle)
    {
        return $seoarticle->id;
    }
}
