<?php

namespace App\Policies;

use App\SeoContact;
use Illuminate\Auth\Access\HandlesAuthorization;

class SeoContactPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(SeoContact $seocontact)
    {
        return $seocontact->id;
    }
}
