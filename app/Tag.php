<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    protected $table = 'tagging_tags';
    protected $fillable = [
        'slug', 'name','count'
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
