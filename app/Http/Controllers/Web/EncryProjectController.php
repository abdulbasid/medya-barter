<?php

namespace App\Http\Controllers\Web;

use App\Project;
use App\Contact;
use App\SeoReferance;
use App\HomePage;
use App\AllTags;
use App\Setting;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EncryProjectController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:customer');
    }

    public function index(){
        //  $encryprojects = Project::All()->Where('statu',2);
        $encryprojects = DB::table('projects')
        ->where('statu', 1)
        ->orWhere('statu', 2)
        ->get();

        $contacts = Contact::All();
        $seoreferances = SeoReferance::All();
        $homepages = SeoReferance::All();
        $hometags = AllTags::All()->where('taggable_type','App\\SeoReferance');
        $settings = Setting::All();

        

    	return view('web.pages.encryprojects', compact('encryprojects','contacts','seoreferances','homepages','hometags','settings'));
    }
}
