<?php

namespace App\Http\Controllers\Web;

use App\Aboutus;
use App\Contact;
use App\SeoAbout;
use App\AllTags;
use App\Setting;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutUsController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(){
        $aboutuses = Aboutus::All();
        $contacts = Contact::All();

        $seoaboutes = SeoAbout::All();
        $seoabouttags = AllTags::All()->where('taggable_type','App\\SeoAbout');

        $settings = Setting::All();

        // $seoabouts = DB::table('tagging_tagged')
        // ->where("taggable_type","App\\SeoAbout")
        // ->get();
        // dd($seoabouts);

    	return view('web.pages.about-us', compact('aboutuses','contacts','seoabouttags','seoaboutes','settings'));
    }
}
