<?php

namespace App\Http\Controllers\Web;

use App\Article;
use App\Contact;
use App\SeoArticle;
use App\AllTags;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(){
        $articles  = Article::orderBy('id', 'DESC')->paginate(8);
        $lastarticles  = Article::orderBy('id', 'DESC')->limit(5);
        $contacts = Contact::All();

        $seoarticles = SeoArticle::All();
        $seoarticletags = AllTags::All()->where('taggable_type','App\\SeoArticle');
        
        $settings = Setting::All();

        return view('web.pages.articles', compact('articles','contacts','seoarticles','lastarticles','seoarticletags','settings'));
    }
}
