<?php

namespace App\Http\Controllers\Web;

use App\Contact;
use App\SeoContact;
use App\HomePage;
use App\AllTags;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(){

        $contacts = Contact::All();

        $seocontacts = SeoContact::All();
        $contacttags = AllTags::All()->where('taggable_type','App\\SeoContact');
        $homepages = HomePage::All();
        $settings = Setting::All();

    	return view('web.pages.contact', compact('contacts','seocontacts','homepages','contacttags','settings'));
    }
}
