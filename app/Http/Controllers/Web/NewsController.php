<?php

namespace App\Http\Controllers\Web;

use App\Project;
use App\Contact;
use App\HomePage;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(){
        $referances = Project::All();
        $contacts = Contact::All();
        $homepages = HomePage::All();
        $settings = Setting::All();


    	return view('web.pages.news', compact('referances','contacts','homepages','settings'));
    }
}
