<?php

namespace App\Http\Controllers\Web;

use App\Project;
use App\Contact;
use App\SeoReferance;
use App\AllTags;
use App\Setting;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReferanceController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function referance(){
        // $referances = Project::All()->where('statu',0)->orWhere(function ($query) {
        //     $query->where('statu', '==', 2);
        // });

        $referances = DB::table('projects')
                    ->where('statu', 0)
                    ->orWhere('statu', 2)
                    ->orderBy('ranking','ASC')
                    ->get();

        $contacts = Contact::All();

        $seoreferances = SeoReferance::All();
        $seoreferancestags = AllTags::All()->where('taggable_type','App\\SeoReferance');
        $settings = Setting::All();

    	return view('web.pages.referances', compact('referances','contacts','seoreferances','seoreferancestags','settings'));
    }
}
