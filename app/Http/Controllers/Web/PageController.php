<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Tag;
use App\Post;
use App\Project;
use App\Slider;
use App\Contact;
use App\WhatDoesIt;
use App\Work;
use App\AllTags;
use App\Article;
use DB;
use App\HomePage;
use App\SeoAbout;
use App\SeoReferance;
use App\SeoContact;
use App\SeoArticle;
use App\Setting;



class PageController extends Controller
{
    
    public function blog(){
        $posts = Post::orderBy('id', 'DESC')->where('status', 'active')->paginate(3);
        $sliders = Slider::All();
        $contacts = Contact::All();
        $whatdoesits = WhatDoesIt::All();
        $works = Work::All();

        $projects = DB::table('projects')
        ->where('statu', 0)
        ->sortBy('ranking')
        ->orWhere('statu', 2)
        ->get();
       
        $homepages = HomePage::All();
        $hometags = AllTags::All()->where('taggable_type','App\\HomePage');
        $seoabouts = SeoAbout::All();
        $seoreferances = SeoReferance::All();
        $seocontacts = SeoContact::All();
        $settings = Setting::All();

    	return view('web.posts', compact('posts','projects','sliders','contacts','whatdoesits','works','homepages','hometags','seoabouts','seoreferances','seocontacts','settings'));
    }

    public function category($slug){
        $category = Category::where('slug', $slug)->pluck('id')->first();

        $posts = Post::where('category_id', $category)
            ->orderBy('id', 'DESC')->where('status', 'active')->paginate(3);

        return view('web.posts', compact('posts'));
    }

    public function tag($slug){ 
        $posts = Post::whereHas('tags', function($query) use ($slug) {
            $query->where('slug', $slug);
        })
        ->orderBy('id', 'DESC')->where('status', 'active')->paginate(3);

        return view('web.posts', compact('posts'));
    }

    public function post($slug){
        $post = Post::where('slug', $slug)->first();
        $lastposts = Post::orderBy('id', 'DESC')->where('status', 'active')->paginate(3);

    	return view('web.post', compact('post','lastposts'));
    }

    public function project($slug){
    	$project = Project::where('slug', $slug)->first();
        $projects = Project::All();
        $contacts = Contact::All();

        $homepages = HomePage::All();

        $projecttags = AllTags::All()->where('taggable_type','App\\Project');

       
        $settings = Setting::All();

    	return view('web.project', compact('project','projects','contacts','projecttags','settings','homepages'));
    }

    public function article($slug){
    	$article = Article::where('slug', $slug)->first();
        $articles = Article::All();
        $contacts = Contact::All();
       
        $lastarticles  = Article::orderBy('id', 'DESC')->limit(4);

        $seoarticles = SeoArticle::All();
        $seoarticletags = AllTags::All()->where('taggable_type','App\\SeoArticle');

        $settings = Setting::All();

    	return view('web.article', compact('article','articles','contacts','seoarticles','seoarticletags','lastarticles','settings'));
    }

}
