<?php

namespace App\Http\Controllers;


use App\Project;
use App\Contact;
use App\SeoReferance;
use App\HomePage;
use DB;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:customer');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $encryprojects = DB::table('projects')
        ->where('statu', 1)
        ->orWhere('statu', 2)
        ->get();

        $contacts = Contact::All();
        $seoreferances = SeoReferance::All();
        $homepages = HomePage::All();
        $hometags = AllTags::All()->where('taggable_type','App\\HomePage');
        
        return view('web.pages.encryprojects', compact('encryprojects','contacts','seoreferances','homepages','hometags'));
    }
}
