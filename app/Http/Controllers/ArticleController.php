<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Article;


class ArticleController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$articles = Article::all();
        return view('article',compact('articles'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	


    	$input = $request->all();
    	$tags = explode(",", $request->tags);


    	$article = Article::create($input);
    	$article->tag($tags);


        return back()->with('success','Article created successfully.');
    }
}
