<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ContactMessagetoreRequest;
use App\Http\Requests\ContactMessageUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\ContactMessage;

class ContactMessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactmessages = ContactMessage::orderBy('id', 'DESC')
            ->paginate();


        return view('admin.contactmessages.index', compact('contactmessages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.contactmessages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactMessagetoreRequest $request)
    {
        $contactmessage = ContactMessage::create($request->all());
        

        return redirect()->route('contactmessages.index', $contactmessage->id)->with('info', 'İletişim Bilgisi Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contactmessage = ContactMessage::find($id);
     

        return view('admin.contactmessages.show', compact('contactmessage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $contactmessage       = ContactMessage::find($id);
      

        return view('admin.contactmessages.edit', compact('contactmessage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactMessageUpdateRequest $request, $id)
    {
        $contactmessage = ContactMessage::find($id);
      

        $contactmessage->fill($request->all())->save();

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $contactmessage->fill(['file' => asset($path)])->save();
        }

        return redirect()->route('contactmessages.index', $contactmessage->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $contactmessage =ContactMessage::where('id',$id)->first();

        if ($contactmessage != null) {
            $contactmessage->delete();
            return redirect()->route('contactmessagess.index')->with(['info'=> 'Başarıyla Silindi']);
        }

        return redirect()->route('contacts.index')->with(['info'=> 'Hatalı ID']);
        }
}
