<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ProjectStoreRequest;
use App\Http\Requests\ProjectUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\Project;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::orderBy('id', 'DESC')
            ->paginate();

        return view('admin.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectStoreRequest $request)
    {
        $tags = explode(",", $request->tags);


    	$project = Project::create($request->all());
    	$project->tag($tags);

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $project->fill(['file' => asset($path)])->save();
        }

      

        return redirect()->route('projects.index', $project->id)->with('info', 'Proje Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::find($id);

        return view('admin.projects.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $project = Project::find($id);
      
        return view('admin.projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectUpdateRequest $request, $id)
    {
        $project = Project::find($id);

        $project->fill($request->all())->save();

        $tags = explode(",", $request->tags);

        $project->tag($tags);

        
        $project->fill($request->all())->save();

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $project->fill(['file' => asset($path)])->save();
        }

        if($request->file('video')){
            $path = Storage::disk('public')->put('image',  $request->file('video'));
            $project->fill(['video' => asset($path)])->save();
        }

        if($request->file('audio')){
            $path = Storage::disk('public')->put('image',  $request->file('audio'));
            $project->fill(['audio' => asset($path)])->save();
        }

        return redirect()->route('projects.index', $project->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $project =Project::where('id',$id)->first();

        if ($project != null) {
            $project->delete();
            return redirect()->route('projects.index')->with(['info'=> 'Başarıyla Silindi']);
        }

        return redirect()->route('projects.index')->with(['info'=> 'Hatalı ID']);
    }
}
