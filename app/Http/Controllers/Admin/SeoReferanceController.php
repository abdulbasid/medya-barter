<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\SeoReferanceStoreRequest;
use App\Http\Requests\SeoReferanceUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\SeoReferance;
use App\Tag;

class SeoReferanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seoreferances = SeoReferance::orderBy('id', 'DESC')
            ->paginate();
            $seoreferancecounts = SeoReferance::All('id')->count();
            

        return view('admin.seoreferances.index', compact('seoreferances','seoreferancecounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.seoreferances.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SeoReferanceStoreRequest $request)
    {
        $tags = explode(",", $request->tags);


    	$seoreferance = SeoReferance::create($request->all());
    	$seoreferance->tag($tags);


        return redirect()->route('seoreferances.index', $seoreferance->id)->with('info', 'Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seoreferance = SeoReferance::find($id);

        return view('admin.seoreferances.show', compact('seoreferance'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seoreferance       = SeoReferance::find($id);

        return view('admin.seoreferances.edit', compact('seoreferance'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SeoReferanceUpdateRequest $request, $id)
    {
        $seoreferance = SeoReferance::find($id);
        $seoreferance->fill($request->all())->save();

        $tags = explode(",", $request->tags);

        $seoreferance->tag($tags);

        

        return redirect()->route('seoreferances.index', $seoreferance->id)->with('info', 'Başarıyla Güncellendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seoreferance = SeoReferance::find($id)->delete();

        return back()->with('info', 'Başarıyla Silindi');
    }
}
