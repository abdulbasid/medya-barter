<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\AboutusStoreRequest;
use App\Http\Requests\AboutusUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\Aboutus;

class AboutusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aboutuses = Aboutus::orderBy('id', 'DESC')
            ->paginate();

        $aboutuscounts = Aboutus::All('id')->count();

        return view('admin.aboutuses.index', compact('aboutuses','aboutuscounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.aboutuses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AboutusStoreRequest $request)
    {
        $aboutus = Aboutus::create($request->all());
      

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $aboutus->fill(['file' => asset($path)])->save();
        }

      

        return redirect()->route('aboutuses.index', $aboutus->id)->with('info', 'Hakkımda Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aboutus = Aboutus::find($id);
       

        return view('admin.aboutuses.show', compact('aboutus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $aboutus       = Aboutus::find($id);
        

        return view('admin.aboutuses.edit', compact('aboutus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AboutusUpdaterequest $request, $id)
    {
        $aboutus = Aboutus::find($id);
       

        $aboutus->fill($request->all())->save();

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $aboutus->fill(['file' => asset($path)])->save();
        }

        return redirect()->route('aboutuses.index', $aboutus->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aboutus =Aboutus::where('id',$id)->first();


        if ($aboutus != null) {
            $aboutus->delete();
            return redirect()->route('aboutuses.index')->with(['info'=> 'Başarıyla Silindi']);
        }

        return redirect()->route('aboutues.index')->with(['info'=> 'Hatalı ID']);
        }
    
}
