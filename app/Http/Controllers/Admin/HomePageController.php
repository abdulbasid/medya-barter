<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\HomePageStoreRequest;
use App\Http\Requests\HomePageUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\HomePage;

class HomePageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $homepages = HomePage::orderBy('id', 'DESC')
            ->paginate();
            $homepagescounts = HomePage::All('id')->count();
            

        return view('admin.homepages.index', compact('homepages','homepagescounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      

        return view('admin.homepages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HomePageStoreRequest $request)
    {

    	$tags = explode(",", $request->tags);


    	$homepage = HomePage::create($request->all());
    	$homepage->tag($tags);
      
        return redirect()->route('homepages.index', $homepage->id)->with('info', 'Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $homepage = HomePage::find($id);

        return view('admin.homepages.show', compact('homepage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
        $homepage       = HomePage::find($id);

        return view('admin.homepages.edit', compact('homepage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HomePageUpdateRequest $request, $id)
    {
        
        $homepage = HomePage::find($id);
        $homepage->fill($request->all())->save();

        $tags = explode(",", $request->tags);

        $homepage->tag($tags);

        return redirect()->route('homepages.index', $homepage->id)->with('info', 'Başarıyla Güncellendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $homepage = HomePage::find($id)->delete();

        return back()->with('info', 'Başarıyla Silindi');
    }
}
