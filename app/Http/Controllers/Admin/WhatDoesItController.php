<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\WhatDoesItStoreRequest;
use App\Http\Requests\WhatDoesItUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\WhatDoesIt;

class WhatDoesItController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $whatdoesits = WhatDoesIt::orderBy('id', 'DESC')
            ->paginate();

        return view('admin.whatdoesits.index', compact('whatdoesits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.whatdoesits.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WhatDoesItStoreRequest $request)
    {
        $whatdoesit = WhatDoesIt::create($request->all());

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $whatdoesit->fill(['file' => asset($path)])->save();
        }

      

        return redirect()->route('whatdoesits.index', $whatdoesit->id)->with('info', 'Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $whatdoesit = WhatDoesIt::find($id);

        return view('admin.whatdoesits.show', compact('whatdoesit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $whatdoesit       = WhatDoesIt::find($id);

        return view('admin.whatdoesits.edit', compact('whatdoesit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WhatDoesItUpdateRequest $request, $id)
    {
        $whatdoesit = WhatDoesIt::find($id);

        $whatdoesit->fill($request->all())->save();

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $whatdoesit->fill(['file' => asset($path)])->save();
        }

        return redirect()->route('whatdoesits.index', $whatdoesit->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $whatdoesit =WhatDoesIt::where('id',$id)->first();

        if ($whatdoesit != null) {
            $whatdoesit->delete();
            return redirect()->route('whatdoesits.index')->with(['info'=> 'Başarıyla Silindi']);
        }

        return redirect()->route('whatdoesits.index')->with(['info'=> 'Hatalı ID']);
        }
}
