<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\SeoAboutStoreRequest;
use App\Http\Requests\SeoAboutUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\SeoAbout;
use App\Tag;

class SeoAboutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seoabouts = SeoAbout::orderBy('id', 'DESC')
            ->paginate();
            $seoaboutcounts = SeoAbout::All('id')->count();
            

        return view('admin.seoabouts.index', compact('seoabouts','seoaboutcounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

        return view('admin.seoabouts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SeoAboutStoreRequest $request)
    {
        $tags = explode(",", $request->tags);


    	$seoabout = SeoAbout::create($request->all());
    	$seoabout->tag($tags);

        return redirect()->route('seoabouts.index', $seoabout->id)->with('info', 'Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seoabout = SeoAbout::find($id);

        return view('admin.seoabouts.show', compact('seoabout'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
        $seoabout       = SeoAbout::find($id);

        return view('admin.seoabouts.edit', compact('seoabout'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SeoAboutUpdateRequest $request, $id)
    {
        $seoabout = SeoAbout::find($id);
        $seoabout->fill($request->all())->save();

        $tags = explode(",", $request->tags);

        $seoabout->tag($tags);

        return redirect()->route('seoabouts.index', $seoabout->id)->with('info', 'Başarıyla Güncellendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seoabout = SeoAbout::find($id)->delete();

        return back()->with('info', 'Başarıyla Silindi');
    }
}
