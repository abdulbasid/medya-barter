<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\SeoContactStoreRequest;
use App\Http\Requests\SeoContactUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\SeoContact;
use App\Tag;

class SeoContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seocontacts = SeoContact::orderBy('id', 'DESC')
            ->paginate();
            $seocontactcounts = SeoContact::All('id')->count();
            

        return view('admin.seocontacts.index', compact('seocontacts','seocontactcounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.seocontacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SeoContactStoreRequest $request)
    {
        $tags = explode(",", $request->tags);


    	$seocontact = SeoContact::create($request->all());
    	$seocontact->tag($tags);


        return redirect()->route('seocontacts.index', $seocontact->id)->with('info', 'Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seocontact = SeoContact::find($id);

        return view('admin.seocontacts.show', compact('seocontact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seocontact       = SeoContact::find($id);

        return view('admin.seocontacts.edit', compact('seocontact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SeoContactUpdateRequest $request, $id)
    {
        $seocontact = SeoContact::find($id);
        $seocontact->fill($request->all())->save();

        $tags = explode(",", $request->tags);

        $seocontact->tag($tags);


        return redirect()->route('seocontacts.index', $seocontact->id)->with('info', 'Başarıyla Güncellendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seocontact = SeoContact::find($id)->delete();

        return back()->with('info', 'Başarıyla Silindi');
    }
}
