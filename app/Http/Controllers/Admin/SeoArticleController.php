<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\SeoArticleStoreRequest;
use App\Http\Requests\SeoArticleUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\SeoArticle;
use App\Tag;

class SeoArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seoarticles = SeoArticle::orderBy('id', 'DESC')
            ->paginate();
            $seoarticlecounts = SeoArticle::All('id')->count();
            

        return view('admin.seoarticles.index', compact('seoarticles','seoarticlecounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.seoarticles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SeoArticleStoreRequest $request)
    {
        $tags = explode(",", $request->tags);


    	$seoarticle = SeoArticle::create($request->all());
    	$seoarticle->tag($tags);


        return redirect()->route('seoarticles.index', $seoarticle->id)->with('info', 'Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seoarticle = SeoArticle::find($id);

        return view('admin.seoarticles.show', compact('seoarticle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seoarticle       = SeoArticle::find($id);

        return view('admin.seoarticles.edit', compact('seoarticle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SeoArticleUpdateRequest $request, $id)
    {
        $seoarticle = SeoArticle::find($id);
        $seoarticle->fill($request->all())->save();

        $tags = explode(",", $request->tags);

        $seoarticle->tag($tags);


        return redirect()->route('seoarticles.index', $seoarticle->id)->with('info', 'Başarıyla Güncellendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seoarticle = SeoArticle::find($id)->delete();

        return back()->with('info', 'Başarıyla Silindi');
    }
}
