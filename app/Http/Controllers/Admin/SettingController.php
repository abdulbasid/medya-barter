<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\SettingStoreRequest;
use App\Http\Requests\SettingUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\Setting;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::orderBy('id', 'DESC')
            ->paginate();
        
        $settingcounts = Setting::All('id')->count();

        return view('admin.settings.index', compact('settings','settingcounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingStoreRequest $request)
    {
      
    	


    	$setting = Setting::create($request->all());
    

        //IMAGE 
        if($request->file('logo')){
            $path = Storage::disk('public')->put('image',  $request->file('logo'));
            $setting->fill(['logo' => asset($path)])->save();
        }

        if($request->file('favicon')){
            $path = Storage::disk('public')->put('image',  $request->file('favicon'));
            $setting->fill(['favicon' => asset($path)])->save();
        }


        return redirect()->route('settings.index', $setting->id)->with('info', 'Başarıyla Oluşturuldu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $setting = Setting::find($id);

        return view('admin.settings.show', compact('setting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $setting       = Setting::find($id);

        return view('admin.settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingUpdateRequest $request, $id)
    {
        $setting = Setting::find($id);
        $setting->fill($request->all())->save();



        //IMAGE 
        if($request->file('logo')){
            $path = Storage::disk('public')->put('image',  $request->file('logo'));
            $setting->fill(['logo' => asset($path)])->save();
        }

        if($request->file('favicon')){
            $path = Storage::disk('public')->put('image',  $request->file('favicon'));
            $setting->fill(['favicon' => asset($path)])->save();
        }


        return redirect()->route('settings.index', $setting->id)->with('info', 'Başarıyla Güncellendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $setting = Setting::find($id)->delete();

        return back()->with('info', 'Başarıyla Silindi');
    }
}
