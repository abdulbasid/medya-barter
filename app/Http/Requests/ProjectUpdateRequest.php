<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'          => 'required',
            'statu'         => 'required',
            'slug'          => 'required|unique:projects,slug,' . $this->project,
        ];

        if($this->get('image'))        
            $rules = array_merge($rules, ['image'         => 'mimes:jpg,jpeg,png']);

        if($this->get('video'))        
            $rules = array_merge($rules, ['image'         => 'mimes:mp4|max:4120']);

        if($this->get('audio'))        
            $rules = array_merge($rules, ['image'         => 'mimes:mp3']);

        return $rules;
    }
}
