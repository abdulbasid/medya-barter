<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use \Conner\Tagging\Taggable;
    
    protected $fillable = [
        'desciription','name','slug','file','video','audio','statu','ranking'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->belongsToMany(AllTags::class);
    }
}
