<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllTags extends Model
{

    protected $table = 'tagging_tagged';
    protected $fillable = [
        'taggable_id', 'taggable_type','tag_name','tag_slug'
    ];
}
