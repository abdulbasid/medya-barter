<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoArticle extends Model
{
    use \Conner\Tagging\Taggable;

    protected $table = 'seoarticles';
    protected $fillable = [
        'title', 'desciription'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
