<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        'name', 'slug' ,'file'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
