<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoAbout extends Model
{
    use \Conner\Tagging\Taggable;

    protected $table = 'seo_abouts';
    protected $fillable = [
        'title', 'desciription'
    ];

    public function tags()
    {
        return $this->belongsToMany(AllTags::class);
    }
}
