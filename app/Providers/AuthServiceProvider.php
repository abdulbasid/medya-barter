<?php

namespace App\Providers;

use App\Post;
use App\Project;
use App\Slider;
use App\Aboutus;
use App\Contact;
use App\WhatDoesIt;
use App\Work;
use App\HomePage;
use App\SeoAbout;
use App\SeoReferance;
use App\SeoContact;
use App\SeoArticle;
use App\Article;
use App\Cutstomer;
use App\ContactMessage;
use App\Policies\PostPolicy;
use App\Policies\ProjectPolicy;
use App\Policies\SliderPolicy;
use App\Policies\AboutusPolicy;
use App\Policies\ContactPolicy;
use App\Policies\WhatDoesItPolicy;
use App\Policies\WorkPolicy;
use App\Policies\HomePagePolicy;
use App\Policies\SeoAboutPolicy;
use App\Policies\SeoReferancePolicy;
use App\Policies\SeoContactPolicy;
use App\Policies\SeoArticlePolicy;
use App\Policies\ArticlePolicy;
use App\Policies\CutstomerPolicy;
use App\Policies\ContactMessagePolicy;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Post::class => PostPolicy::class,
        Project::class => ProjectPolicy::class,
        Slider::class => SliderPolicy::class,
        Aboutus::class => AboutusPolicy::class,
        Contact::class => ContactPolicy::class,
        WhatDoesIt::class => WhatDoesItPolicy::class,
        Work::class => WorkPolicy::class,
        HomePage::class => HomePagePolicy::class,
        SeoAbout::class => SeoAboutPolicy::class,
        SeoReferance::class => SeoReferancePolicy::class,
        SeoContact::class => SeoContactPolicy::class,
        Article::class => ArticlePolicy::class,
        Cutstomer::class => CutstomerPolicy::class,
        SeoArticle::class => SeoArticlePolicy::class,
        ContactMessage::class => ContactMessagePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
