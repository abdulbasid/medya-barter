<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhatDoesIt extends Model
{   
    protected $table = 'whatdoesits';

    protected $fillable = [
        'icon' ,'title','desciription','file'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
