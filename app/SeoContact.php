<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoContact extends Model
{
    use \Conner\Tagging\Taggable;

    protected $table = 'seo_contacts';
    protected $fillable = [
        'title', 'desciription'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
