<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{   
    protected $table = 'works';

    protected $fillable = [
        'icon' ,'title','desciription','file'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
