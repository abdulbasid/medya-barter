@extends('layouts.login')

@section('content')

<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url({{ asset('admin/media//bg/bg-3.jpg') }});">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            <img src="{{ asset('web/images/medyabarter.png') }}">
                        </a>
                    </div>
                    <div class="kt-login__signin">
                    **Referanslarımızı görüntülemek için e-mail adresinize gönderilen şifre ile giriş yapınız..
                        <form class="kt-form" method="POST" action="{{ route('customer.login.submit') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="email" class="form-control" placeholder="E-Mail Address" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" type="password" placeholder="Şifre" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
             
                            <div class="kt-login__actions">
                                <button  type="submit" class="btn btn-brand btn-elevate kt-login__btn-primary">Görüntüle</button>
                            </div>
                        </form>
                    </div>
             
                </div>
            </div>
        </div>
    </div>
</div>
        

@endsection
