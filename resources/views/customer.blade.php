@extends('layouts.inside')

@section('content')

<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

    <!-- begin:: Content Head -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Medya Barter Kullanıcı Panel</h3>
        </div>
        <div class="kt-subheader__toolbar">
            
        </div>
    </div>

    <!-- end:: Content Head -->

    <!-- begin:: Content -->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

  

    

        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">

                <!--begin:: Widgets/Best Sellers-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                İşlem Seçiniz
                            </h3>
                        </div>
                   
                    </div>
                 
                </div>

                <!--end:: Widgets/Best Sellers-->
            </div>
           
        </div>
    </div>

    <!-- end:: Content -->
</div>

@endsection
