<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="TR">
<head>
  
@foreach($settings as $setting)
   	<title>{{ $setting->title }}</title>
@endforeach
	<meta charset="UTF-8">
	<META HTTP-EQUIV='Expires' CONTENT='-1'>
	<META HTTP-EQUIV='Cache-Control' CONTENT='no-cache'>
	<META HTTP-EQUIV='Pragma' CONTENT='no-cache'>
	@if (\Request::is('welcome') || \Request::is('welcome/*'))
	
			@if($hometags->count())
				@foreach($homepages as $key => $homepage)
					<META NAME='Title' CONTENT="{{$homepage->title}}">
					<META NAME='Description' CONTENT="{{$homepage->desciription}}">	
					<META NAME='Keywords' LANG='tr' CONTENT="@foreach($homepage->tags as $tag){{$tag->name}},@endforeach">	

				@endforeach
			@endif
	@elseif (\Request::is('about-us') || \Request::is('about-us/*'))
	
		@if($seoabouttags->count())
			@foreach($seoaboutes as $key => $seoabout)
				<META NAME='Title' CONTENT="{{$seoabout->title}}">
				<META NAME='Description' CONTENT="{{$seoabout->desciription}}">	
				<META NAME='Keywords' LANG='tr' CONTENT="@foreach($seoabout->tags as $tag){{$tag->name}},@endforeach">	

			@endforeach
		@endif

	@elseif (\Request::is('referances') || \Request::is('referances/*'))
	
	@if($seoreferancestags->count())
		@foreach($seoreferances as $key => $seoreferance)
			<META NAME='Title' CONTENT="{{$seoreferance->title}}">
			<META NAME='Description' CONTENT="{{$seoreferance->desciription}}">	
			<META NAME='Keywords' LANG='tr' CONTENT="@foreach($seoreferance->tags as $tag){{$tag->name}},@endforeach">	

		@endforeach
	@endif

	@elseif (\Request::is('articles') || \Request::is('articles/*'))
	
	@if($seoarticletags->count())
		@foreach($seoarticles as $key => $seoarticle)
			<META NAME='Title' CONTENT="{{$seoarticle->title}}">
			<META NAME='Description' CONTENT="{{$seoarticle->desciription}}">	
			<META NAME='Keywords' LANG='tr' CONTENT="@foreach($seoarticle->tags as $tag){{$tag->name}},@endforeach">	

		@endforeach
	@endif

	@elseif (\Request::is('contact') || \Request::is('contact/*'))
	
	@if($contacttags->count())
		@foreach($seocontacts as $key => $seocontact)
			<META NAME='Title' CONTENT="{{$seocontact->title}}">
			<META NAME='Description' CONTENT="{{$seocontact->desciription}}">	
			<META NAME='Keywords' LANG='tr' CONTENT="@foreach($seocontact->tags as $tag){{$tag->name}},@endforeach">	

		@endforeach
	@endif

	@elseif (\Request::is('project') || \Request::is('project/*'))
	
	@if($projecttags->count())
		@foreach($homepages as $key => $homepage)
			<META NAME='Title' CONTENT="{{$homepage->name}}">
			<META NAME='Description' CONTENT="{{$homepage->desciription}}">	
			<META NAME='Keywords' LANG='tr' CONTENT="@foreach($homepage->tags as $tag){{$tag->name}},@endforeach">	

		@endforeach
	@endif

	@elseif (\Request::is('encryprojects') || \Request::is('encryprojects/*'))
	
	@if($hometags->count())
		@foreach($homepages as $key => $homepage)
			<META NAME='Title' CONTENT="{{$homepage->name}}">
			<META NAME='Description' CONTENT="{{$homepage->desciription}}">	
			<META NAME='Keywords' LANG='tr' CONTENT="@foreach($homepage->tags as $tag){{$tag->name}},@endforeach">	

		@endforeach
	@endif

	@elseif (\Request::is('article') || \Request::is('article/*'))
	
	@if($seoarticletags->count())
		@foreach($seoarticles as $key => $seoarticle)
			<META NAME='Title' CONTENT="{{$seoarticle->name}}">
			<META NAME='Description' CONTENT="{{$seoarticle->desciription}}">	
			<META NAME='Keywords' LANG='tr' CONTENT="@foreach($seoarticle->tags as $tag){{$tag->name}},@endforeach">	

		@endforeach
	@endif

	@else

	@if($hometags->count())
		@foreach($homepages as $key => $homepage)
			<META NAME='Title' CONTENT="{{$homepage->title}}">
			<META NAME='Description' CONTENT="{{$homepage->desciription}}">	
			<META NAME='Keywords' LANG='tr' CONTENT="@foreach($homepage->tags as $tag){{$tag->name}},@endforeach">	

		@endforeach
	@endif
		
	@endif
	<META NAME='Publisher' CONTENT='Emre Bora - http://www.medyabarter.com.tr'>
	<META NAME='Identifier-URL' CONTENT='http://www.google.com'>
	<META NAME='Copyright' CONTENT="© 2003 - 2019 Medya Barter A.Ş.">
	<META NAME='Reply-to' CONTENT='info@medyabarter.com.tr'>
	<META NAME='Robots' CONTENT='All'>
	<META NAME='Revisit-after' CONTENT='15'>
  
    <!-- Favicon -->

	@foreach($settings as $setting)
		@if($setting->favicon)
			<link rel="shortcut icon" href="{{ $setting->favicon }}">
		@endif
	@endforeach

    
    
	<!-- Responsive Metatag -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    @include('web.vendor.styles')

    @include('web.vendor.scripts')
   
	
	
	
</head>
<body>

@include('web.header')

@yield('content')

@include('web.footer')
	
	
  
</body>
</html>