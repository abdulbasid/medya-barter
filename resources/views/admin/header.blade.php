
 <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">


<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
    <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
       
    </div>
</div>


<div class="kt-header__topbar">

    <div class="kt-header__topbar-item kt-header__topbar-item--user">
        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
            <div class="kt-header__topbar-user">
                <span class="kt-header__topbar-welcome kt-hidden-mobile">Merhaba,</span>
                <span class="kt-header__topbar-username kt-hidden-mobile"> {{ucfirst(auth()->user()->name)}}</span>
                <img class="kt-hidden" alt="Pic" src="{{ asset('admin/media/users/300_25.jpg')}}" />
                <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{strtoupper(substr(auth()->user()->name, 0, 1))}}</span>
            </div>
        </div>
        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

            <!--begin: Head -->
            <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url({{ asset('admin/media/misc/bg-1.jpg') }})">
                <div class="kt-user-card__avatar">
                    <img class="kt-hidden" alt="Pic" src="{{ asset('admin/media/users/300_25.jpg') }} " />

                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                    <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">{{strtoupper(substr(auth()->user()->name, 0, 1))}}</span>
                </div>
                <div class="kt-user-card__name">
                {{ucfirst(auth()->user()->name)}}
                </div>
            </div>
            <div class="kt-notification">
                <div class="kt-notification__custom">
                   <a href="{{ route('logout') }}" class="btn btn-label-brand btn-sm btn-bold">Çıkış</a>
                </div>
            </div>
        </div>
    </div>

  
</div>

</div>


