{{ Form::hidden('user_id', auth()->user()->id) }}


<div class="form-group">
    {{ Form::label('title', 'Başlık') }}
    {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) }}
</div>
<div class="form-group">
    {{ Form::label('desciription', 'Açıklama') }}
    {{ Form::textarea('desciription', null, ['class' => 'form-control', 'id' => 'desciription']) }}
</div>
<div class="form-group">
    {{ Form::label('image', 'Resim') }}
    {{ Form::file('image') }}
</div>

<div class="form-group">
{{ Form::submit('Ekle', ['class' => 'btn btn-primary btn-wide col-md-12 ']) }}
</div>

