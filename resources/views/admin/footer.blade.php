<!-- begin:: Footer -->
<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
    <div class="kt-footer__copyright">
        2019&nbsp;&copy;&nbsp;<a href="https://beyinatolyesi.com/" target="_blank" class="kt-link">Beyin Atölyesi</a>
    </div>
    <div class="kt-footer__menu">
        <a href="https://beyinatolyesi.com/" target="_blank" class="kt-footer__menu-link kt-link">Hakkımızda</a>
        <a href="https://beyinatolyesi.com/" target="_blank" class="kt-footer__menu-link kt-link">İletişim</a>
    </div>
</div>

<!-- end:: Footer -->