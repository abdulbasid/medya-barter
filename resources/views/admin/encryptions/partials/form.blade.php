{{ Form::hidden('user_id', auth()->user()->id) }}

<div class="form-group" name="myform" >
    {{ Form::label('password', 'Şifre') }}
    {{ Form::text('password', null, ['class' => 'form-control', 'id' => 'password','name' => 'password' ]) }}<br />
    <input type="button" value="Şifre Oluştur" class="btn btn-warning btn-wide col-md-12" onClick="populateform(this.form.thelength.value)"><br />
    
    <b><br />Şifre Uzunluğu:</b> <input type="text" name="thelength" size=3 value="7">
    
</div>

<div class="form-group" >
    {{ Form::label('email', 'Email') }}
    {{ Form::text('email', null, ['class' => 'form-control', 'id' => 'email']) }}
</div>

<div class="form-group" >
    {{ Form::label('enddate', 'Bitiş Tarihi') }}
    {{ Form::text('enddate', null, ['class' => 'form-control', 'id' => 'kt_datepicker_4_2']) }}
</div>

<div class="form-group">
{{ Form::submit('Ekle', ['class' => 'btn btn-primary btn-wide col-md-12 ']) }}
</div>

<script>

    //Random password generator- by javascriptkit.com
    //Visit JavaScript Kit (http://javascriptkit.com) for script
    //Credit must stay intact for use

    var keylist="abcdefghijklmnopqrstuvwxyz123456789"
    var temp=''

    function generatepass(plength){
    temp=''
    for (i=0;i<plength;i++)
    temp+=keylist.charAt(Math.floor(Math.random()*keylist.length))
    return temp
    }

    function populateform(enterlength){
    document.pgenerate.password.value=generatepass(enterlength)
    }
</script>