@extends('layouts.inside')

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">
          Panel </h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="{{ route('aboutuses.index') }}" class="kt-subheader__breadcrumbs-link">
                Ne İş Yaparız? </a>
            

            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>
    </div>
    <div class="kt-subheader__toolbar">
        <button class="btn btn-danger" onclick="deleteItems()">Cache Temizle   </button>
    </div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Ne iş Yaparız?
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{{ route('whatdoesits.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                           Yeni Ekle
                        </a>
                    </div>
                </div>
            </div>
          
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                    <tr>
                        <th width="10px">ID</th>
                        <th>İcon</th>
                        <th>Başlık</th>
                        <th>Açıklama</th>
                        <th colspan="3">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($whatdoesits as $whatdoesit)
                    <tr>
                        <td>{{ $whatdoesit->id }}</td>
                        <td>{{ $whatdoesit->icon }}</td>
                        <td>{{ $whatdoesit->title }}</td>
                        <td>{{ $whatdoesit->desciription }}</td>
                        <td width="10px">
                            <a href="{{ route('whatdoesits.show', $whatdoesit->id) }}" class="btn btn-sm btn-default">Görüntüle</a>
                        </td>
                        <td width="10px">
                            <a href="{{ route('whatdoesits.edit', $whatdoesit->id) }}" class="btn btn-sm btn-default">Düzenle</a>
                        </td>
                        <td width="10px">
                            {!! Form::open(['route' => ['whatdoesits.destroy', $whatdoesit->id], 'method' => 'DELETE']) !!}
                                <button class="btn btn-sm btn-danger">
                                    Sil
                                </button>                           
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $whatdoesits->render() }}
            <!--end: Datatable -->
        </div>
    </div>
</div>




@endsection