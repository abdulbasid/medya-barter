@extends('layouts.inside')

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">
            Panel </h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="{{ route('contacts.index') }}" class="kt-subheader__breadcrumbs-link">
                İletişim </a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="" class="kt-subheader__breadcrumbs-link">
                İletişim Görüntüle </a>

        </div>
    </div>
    <div class="kt-subheader__toolbar">
        <button class="btn btn-danger" onclick="deleteItems()">Cache Temizle   </button>
    </div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile">
                <div class="kt-portlet__head kt-portlet__head--lg" style="">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">İletişim <small>Görüntüle</small></h3>
                    </div>
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-toolbar">
                            <a href="{{ url()->previous() }}" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Geri</span>
                            </a>
                        </div>
                    </div>
                </div>
               
                <div class="kt-portlet__body">
                    <table class="table table-bordered">
                        <tbody><tr>
                            <td style="width: 50%"><strong>Başlık</strong></td>
                            <td>
                            {{ $contact->address }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><strong>E-Mail</strong></td>
                            <td>
                            {{ $contact->email }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><strong>Telefon</strong></td>
                            <td>
                            {{ $contact->gsm }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><strong>Telefon 2</strong></td>
                            <td>
                            {{ $contact->gsm2 }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><strong>Harita</strong></td>
                            <td>
                            <div class="kt-section__content">
                                <p style="padding: 20px; margin: 10px 0 30px 0; border: 4px solid #efefef" id="kt_blockui_1_content">
                                {{ $contact->map }}
                                </p>
                            </div>
                           
                            </td>
                        </tr>
                       
                    </tbody>
                </table>
                </div>


            </div>
        </div>
    </div>
</div>
@endsection
