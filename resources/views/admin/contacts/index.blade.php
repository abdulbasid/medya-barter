@extends('layouts.inside')

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">
          Panel </h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="{{ route('contacts.index') }}" class="kt-subheader__breadcrumbs-link">
                İletişim Bilgisi </a>
            

            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>
    </div>
    <div class="kt-subheader__toolbar">
        <button class="btn btn-danger" onclick="deleteItems()">Cache Temizle   </button>
    </div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    İletişim Bilgileri
                </h3>
            </div>
            @if($contactcounts < 1)
            <div class="kt-portlet__head-toolbar" style="visibility: hidden">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{{ route('contacts.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                           Yeni İletişim Bilgisi
                        </a>
                    </div>
                </div>
            </div>
            else
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{{ route('contacts.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                           Yeni İletişim Bilgisi
                        </a>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Adres</th>
                        <th>E-Mail</th>
                        <th>Telefon</th>
                        <th>Telefon 2</th>
                        <th colspan="3">işlemler</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($contacts as $contact)
                    <tr>
                        <td>{{ $contact->id }}</td>
                        <td>{{ $contact->address }}</td>
                        <td>{{ $contact->email }}</td>
                        <td>{{ $contact->gsm }}</td>
                        <td>{{ $contact->gsm2 }}</td>
                        <td width="10px" >
                            <a href="{{ route('contacts.show', $contact->id) }}" class="btn btn-sm btn-default">Görüntüle</a>
                        </td>
                        <td width="10px">
                            <a href="{{ route('contacts.edit', $contact->id) }}" class="btn btn-sm btn-default">Düzenle</a>
                        </td>
                        <td width="10px">
                            {!! Form::open(['route' => ['contacts.destroy', $contact->id], 'method' => 'DELETE']) !!}
                                <button class="btn btn-sm btn-danger">
                                    Sil
                                </button>                           
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>

@endsection
