@extends('layouts.inside')

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">
            Panel </h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="{{ route('tags.index') }}" class="kt-subheader__breadcrumbs-link">
            Etiketler </a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="" class="kt-subheader__breadcrumbs-link">
            Etiket Görüntüle </a>

        </div>
    </div>
  
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile">
                <div class="kt-portlet__head kt-portlet__head--lg" style="">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Etiket <small>Görüntüle</small></h3>
                    </div>
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-toolbar">
                            <a href="{{ url()->previous() }}" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Geri</span>
                            </a>
                        </div>
                    </div>
                </div>
               
                <div class="kt-portlet__body">
                    <table class="table table-bordered">
                        <tbody><tr>
                            <td style="width: 30%"><strong>Etiket Adı</strong></td>
                            <td>
                             {{ $tag->name }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%"><strong>Url</strong></td>
                            <td>
                            {{ $tag->slug }}
                            </td>
                        </tr>
                       
                    </tbody>
                </table>
                </div>


            </div>
        </div>
    </div>
</div>
@endsection
