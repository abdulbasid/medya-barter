{{ Form::hidden('user_id', auth()->user()->id) }}
<div class="row">
	<div class="col-md-6">
		<br>
        <div class="form-group">
            {{ Form::label('statu', 'Durum') }}
            {!! Form::select('statu', array('0' => 'Herkes', '1' => 'Özel', '2' => 'Herkes ve Özel'), '0',['class' => 'form-control', 'id' => 'statu']); !!}
        </div>

        <div class="form-group">
            {{ Form::label('name', 'Proje Adı : ',['class' => ' col-form-label']) }}
            {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
        </div>

        <div class="form-group">
            {{ Form::label('slug', 'Sıralama : ',['class' => ' col-form-label']) }}
            {{ Form::text('ranking', null, ['class' => 'form-control', 'id' => 'ranking']) }}
        </div>
        <div class="form-group">
            {{ Form::label('slug', 'URL : ',['class' => ' col-form-label']) }}
            {{ Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
        </div>

        <div class="form-group">
			<label>Etiketler:</label>
			<br/>
			<input data-role="tagsinput" type="text" name="tags" >
			@if ($errors->has('tags'))
                <span class="text-danger">{{ $errors->first('tags') }}</span>
            @endif
		</div>
		
	</div>

    <div class="col-md-6">
		<br>
        <div class="form-group">
            {{ Form::label('desciription', 'Açıklama : ',['class' => ' col-form-label']) }}
            {{ Form::textarea('desciription', null, ['class' => 'form-control', 'id' => 'desciription']) }}
        </div>
	
		

        <div class="form-group">
            {{ Form::label('image', 'Resim : ',['class' => ' col-form-label']) }}
            {{ Form::file('image') }}
        </div>

        <div class="form-group">
            {{ Form::label('video', 'Video : ',['class' => ' col-form-label']) }}
            {{ Form::file('video') }}
        </div>

        <div class="form-group">
            {{ Form::label('audio', 'Audio : ',['class' => ' col-form-label']) }}
            {{ Form::file('audio') }}
        </div>

        <div class="form-group">
            {{ Form::submit('Ekle', ['class' => 'btn btn-primary btn-wide col-md-12 ']) }}
        </div>

	</div>

</div>




