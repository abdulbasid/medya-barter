<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>

<!-- end::Global Config -->

<!--begin:: Global Mandatory Vendors -->
<script src="{{ asset('admin/vendors/general/jquery/dist/jquery.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/popper.js/dist/umd/popper.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/js-cookie/src/js.cookie.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/moment/min/moment.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/tooltip.js/dist/umd/tooltip.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/sticky-js/dist/sticky.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/wnumb/wNumb.js') }}" type="text/javascript">
</script>

<!--end:: Global Mandatory Vendors -->

<!--begin:: Global Optional Vendors -->
<script src="{{ asset('admin/vendors/general/jquery-form/dist/jquery.form.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/block-ui/jquery.blockUI.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/custom/components/vendors/bootstrap-datepicker/init.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/custom/components/vendors/bootstrap-timepicker/init.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/bootstrap-select/dist/js/bootstrap-select.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/custom/components/vendors/bootstrap-switch/init.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/select2/dist/js/select2.full.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/ion-rangeslider/js/ion.rangeSlider.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/typeahead.js/dist/typeahead.bundle.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/handlebars/dist/handlebars.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/inputmask/dist/jquery.inputmask.bundle.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/nouislider/distribute/nouislider.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/owl.carousel/dist/owl.carousel.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/autosize/dist/autosize.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/clipboard/dist/clipboard.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/dropzone/dist/dropzone.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/summernote/dist/summernote.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/markdown/lib/markdown.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/custom/components/vendors/bootstrap-markdown/init.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/bootstrap-notify/bootstrap-notify.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/custom/components/vendors/bootstrap-notify/init.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/jquery-validation/dist/jquery.validate.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/jquery-validation/dist/additional-methods.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/custom/components/vendors/jquery-validation/init.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/toastr/build/toastr.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/raphael/raphael.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/morris.js/morris.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/chart.js/dist/Chart.bundle.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/waypoints/lib/jquery.waypoints.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/counterup/jquery.counterup.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/es6-promise-polyfill/promise.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/sweetalert2/dist/sweetalert2.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/custom/components/vendors/sweetalert2/init.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/jquery.repeater/src/lib.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/jquery.repeater/src/jquery.input.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/jquery.repeater/src/repeater.js') }}" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/general/dompurify/dist/purify.js') }}" type="text/javascript">
</script>

<!--end:: Global Optional Vendors -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="{{ asset('admin/demo/default/base/scripts.bundle.js') }}" type="text/javascript">
</script>
<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
<script src="{{ asset('admin/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript">
</script>
<!--end::Page Vendors -->


<!--begin::Page Scripts(used by this page) -->
<script src="{{ asset('admin/app/custom/general/crud/datatables/advanced/column-rendering.js') }}" type="text/javascript">
</script>

<!--end::Page Scripts -->


<script src="{{ asset('admin/app/custom/general/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>

<!--begin::Global App Bundle(used by all pages) -->
<script src="{{ asset('admin/app/bundle/app.bundle.js') }}" type="text/javascript">
</script>
<!--end::Global App Bundle -->

<!--begin::Page Vendors(used by this page) -->
<script src="{{ asset('admin/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript">
</script>
<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript">
</script>
<script src="{{ asset('admin/vendors/custom/gmaps/gmaps.js') }}" type="text/javascript">
</script>
<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{ asset('admin/app/custom/general/crud/forms/widgets/bootstrap-daterangepicker.js') }}" type="text/javascript">
</script>

<!--end::Page Scripts -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{ asset('admin/app/custom/general/dashboard.js') }}" type="text/javascript">
</script>

<!--end::Page Scripts -->


<!--begin::Page Scripts(used by this page) -->
<script src="{{ asset('admin/app/custom/general/crud/forms/widgets/input-mask.js') }}" type="text/javascript">
</script>

<!--end::Page Scripts -->


<!--begin::Page Scripts(used by this page) -->
<script src="{{ asset('admin/app/custom/general/crud/forms/widgets/dropzone.js') }}" type="text/javascript">
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>

<!--end::Page Scripts -->
    <!--end::Global App Bundle -->


    <script src="{{ asset('vendor/stringToSlug/jquery.stringToSlug.min.js') }}"></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script>
	$(document).ready(function(){
	    $("#name, #slug").stringToSlug({
	        callback: function(text){
	            $('#slug').val(text);
	        }
	    });

	    CKEDITOR.config.height = 400;
		CKEDITOR.config.width  = 'auto';

		CKEDITOR.replace('body');
	});
</script>

<script type="text/javascript">
      
    //   $(document).ready(function () {
    //     $('#mySelect').change(function () {
    //         if ($('#mySelect').val() == '1') {
    //             $('#link').show();
    //             $('#name').hide();
    //         }
    //         else {
    //             $('#link').hide();
    //             $('#name').show();
    //         }
    //     });
    // });

    function showDiv(){
        getSelectValue = document.getElementById("type").value;
        if(getSelectValue == "0"){
            document.getElementById("link").style.display="none";
            document.getElementById("resim").style.display="block";
           
        }else{
            document.getElementById("link").style.display="block";
            document.getElementById("resim").style.display="none";
        }
    }


</script>

<script>

function deleteItems() {
  localStorage.clear();
}

</script>