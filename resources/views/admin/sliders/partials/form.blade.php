{{ Form::hidden('user_id', auth()->user()->id) }}

<div class="form-group" >
    {{ Form::label('name', 'Slider Adı') }}
    {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
</div>

<div class="form-group">
    {{ Form::label('slug', 'URL') }}
    {{ Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
</div>
<div class="form-group" id="resim">
    {{ Form::label('image', 'Resim') }}
    {{ Form::file('image') }}
</div>

<div class="form-group">
{{ Form::submit('Ekle', ['class' => 'btn btn-primary btn-wide col-md-12 ']) }}
</div>

