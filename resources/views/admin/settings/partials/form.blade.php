{{ Form::hidden('user_id', auth()->user()->id) }}


<div class="row">
	<div class="col-md-6">
		<br>

		<div class="form-group">
			{{ Form::label('title', 'Site Başlığı') }}
			{{ Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) }}
		</div>

		<div class="form-group">
			{{ Form::label('image', 'Logo') }}
			{{ Form::file('logo') }}
		</div>

		
		<div class="form-group">
			{{ Form::label('logoalt', 'Logo Alt') }}
			{{ Form::text('logoalt', null, ['class' => 'form-control', 'id' => 'logoalt']) }}
		</div>

		<div class="form-group">
			{{ Form::label('image', 'Favicon') }}
			{{ Form::file('favicon') }}
		</div>
		
		<div class="form-group">
			{{ Form::submit('Ekle', ['class' => 'btn btn-sm btn-primary']) }}
		</div>
	</div>

</div>







@section('scripts')
<script src="{{ asset('vendor/stringToSlug/jquery.stringToSlug.min.js') }}"></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script>
	$(document).ready(function(){
	    $("#name, #slug").stringToSlug({
	        callback: function(text){
	            $('#slug').val(text);
	        }
	    });

	    CKEDITOR.config.height = 400;
		CKEDITOR.config.width  = 'auto';

		CKEDITOR.replace('body');
	});
</script>



@endsection