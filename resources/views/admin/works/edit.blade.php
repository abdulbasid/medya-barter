@extends('layouts.inside')

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">
          Panel </h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="{{ route('works.index') }}" class="kt-subheader__breadcrumbs-link">
            Neler Yaparız? </a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="" class="kt-subheader__breadcrumbs-link">
               Neler Yaparız? Güncelle </a>
        </div>
    </div>
    <div class="kt-subheader__toolbar">
        <button class="btn btn-danger" onclick="deleteItems()">Cache Temizle   </button>
    </div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-lg-12">
            
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                        Neler Yaparız? <small>Güncelle</small>
                        </h3>
                    </div>
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-toolbar">
                            <a href="{{ url()->previous() }}" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Geri</span>
                            </a>
                        </div>
                    </div>
                </div>

                <!--begin::Form-->
                <div class="row justify-content-center">
                {!! Form::model($work, ['route' => ['works.update', $work->id], 'method' => 'PUT', 'files' => true]) !!}
                        
                    @include('admin.works.partials.form')

                {!! Form::close() !!}
                </div>

                <!--end::Form-->
            </div>
            
        </div>
    </div>
</div>

@endsection
