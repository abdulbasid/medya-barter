{{ Form::hidden('user_id', auth()->user()->id) }}


<div class="row">
	<div class="col-md-6">
		<br>

		<div class="form-group">
			{{ Form::label('name', 'Makale Başlığı') }}
			{{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
		</div>
		<div class="form-group">
			{{ Form::label('slug', 'URL') }}
			{{ Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
		</div>


		<div class="form-group">
		{{ Form::label('startdate', 'Paylaşım Tarihi') }}
		{{ Form::text('startdate', null, ['class' => 'form-control', 'id' => 'kt_datepicker_4_2']) }}
		</div>

		<div class="form-group">
			<label>Tags:</label>
			<br/>
			<input data-role="tagsinput" type="text" name="tags" >
			@if ($errors->has('tags'))
                <span class="text-danger">{{ $errors->first('tags') }}</span>
            @endif
		</div>
	</div>

	<div class="col-md-6">
	<br>
		<div class="form-group">
			{{ Form::label('image', 'Resim') }}
			{{ Form::file('image') }}
		</div>
		<div class="form-group">
			{{ Form::label('body', 'Açıklama') }}
			{{ Form::textarea('body', null, ['class' => 'form-control']) }}
		</div>

		<div class="form-group">
			{{ Form::label('status', 'Durum') }}
			<label>
				{{ Form::radio('status', 'active') }} Aktif
			</label>
			<label>
				{{ Form::radio('status', 'passive') }} Pasif
			</label>
		</div>

		<div class="form-group">
			{{ Form::submit('Ekle', ['class' => 'btn btn-sm btn-primary']) }}
		</div>
	</div>
</div>







@section('scripts')
<script src="{{ asset('vendor/stringToSlug/jquery.stringToSlug.min.js') }}"></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script>
	$(document).ready(function(){
	    $("#name, #slug").stringToSlug({
	        callback: function(text){
	            $('#slug').val(text);
	        }
	    });

	    CKEDITOR.config.height = 400;
		CKEDITOR.config.width  = 'auto';

		CKEDITOR.replace('body');
	});
</script>



@endsection