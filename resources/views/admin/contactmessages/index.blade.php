@extends('layouts.inside')

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">
          Panel </h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="{{ route('contactmessages.index') }}" class="kt-subheader__breadcrumbs-link">
                Gelen Mesajlar </a>
            

            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>
    </div>
    <div class="kt-subheader__toolbar">
        <button class="btn btn-danger" onclick="deleteItems()">Cache Temizle   </button>
    </div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Gelen Mesajlar
                </h3>
            </div>

        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>İsim</th>
                        <th>E-Mail</th>
                        <th>Mesaj</th>
                        <th colspan="3">işlemler</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($contactmessages as $contactmessage)
                    <tr>
                        <td>{{ $contactmessage->id }}</td>
                        <td>{{ $contactmessage->name }}</td>
                        <td>{{ $contactmessage->email }}</td>
                        <td>{{ $contactmessage->message }}</td>
                        <td width="10px" >
                            <a href="{{ route('contactmessages.show', $contactmessage->id) }}" class="btn btn-sm btn-default">Görüntüle</a>
                        </td>
                        <td width="10px">
                            <a href="{{ route('contactmessages.edit', $contactmessage->id) }}" class="btn btn-sm btn-default">Düzenle</a>
                        </td>
                        <td width="10px">
                            {!! Form::open(['route' => ['contactmessages.destroy', $contactmessage->id], 'method' => 'DELETE']) !!}
                                <button class="btn btn-sm btn-danger">
                                    Sil
                                </button>                           
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>

@endsection
