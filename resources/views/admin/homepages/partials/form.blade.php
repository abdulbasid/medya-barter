{{ Form::hidden('user_id', auth()->user()->id) }}


<div class="row">
	<div class="col-md-6">
		<br>
		<div class="form-group">
			{{ Form::label('title', 'Açıklama') }}
			{{ Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) }}
		</div>

		<div class="form-group">
			{{ Form::label('desciription', 'Açıklama') }}
			{{ Form::textarea('desciription', null, ['class' => 'form-control', 'id' => 'desciription']) }}
		</div>
	
		<div class="form-group">
			<label>Etiketler:</label>
			<br/>
			<input data-role="tagsinput" type="text" name="tags" >
			@if ($errors->has('tags'))
                <span class="text-danger">{{ $errors->first('tags') }}</span>
            @endif
		</div>

		<div class="form-group">
			{{ Form::submit('Ekle', ['class' => 'btn btn-sm btn-primary']) }}
		</div>
			
	</div>
</div>




@section('scripts')
<script src="{{ asset('vendor/stringToSlug/jquery.stringToSlug.min.js') }}"></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script>
	$(document).ready(function(){
	    $("#name, #slug").stringToSlug({
	        callback: function(text){
	            $('#slug').val(text);
	        }
	    });

	    CKEDITOR.config.height = 400;
		CKEDITOR.config.width  = 'auto';

		CKEDITOR.replace('body');
	});
</script>
@endsection