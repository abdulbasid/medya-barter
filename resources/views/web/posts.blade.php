@extends('layouts.outside')

@section('content')
<div class="container" id="container">

@include('web.slider')
          
	
          <div id="content">
  
              <div class="container">
              
                  <div class="hr5" style="margin-top:25px; margin-bottom:45px;"></div>
  
                  @include('web.what-about')
  
                   
                  <div class="hr5" style="margin-top:55px; margin-bottom:55px;"></div>
  
                  @include('web.projects')
                  
                  <div class="hr5" style="margin-top:50px; margin-bottom:55px;"></div>
                  
                
             </div>
          </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

       

        	@foreach($posts as $post)
            <div class="panel panel-default">
                <div class="panel-heading">{{ $post->name }}</div>

                <div class="panel-body">
                    @if($post->file)
                        <img src="{{ $post->file }}" class="img-responsive">
                    @endif
                    
                    {{ $post->excerpt }}
                    <a href="{{ route('post', $post->slug) }}" class="pull-right">Leer más</a>
                </div>
            </div>
            @endforeach

            {{ $posts->render() }}
        </div>
    </div>
</div>

@endsection
