<!-- Start Header -->
<div class="hidden-header"></div>
<header class="clearfix">
    
    <!-- Start Top Bar -->
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                        <!-- Start Contact Info -->
                    <ul class="contact-details">
                    @foreach($contacts as $contact)
                    <li><a href="#"><i class="icon-mobile-2"></i> {{ $contact->gsm }}</a></li>
                    @endforeach  
                    @foreach($contacts as $contact)
                    <li><a href="#"><i class="icon-mail-2"></i> {{ $contact->email }}</a></li>
                    @endforeach    
                    </ul>
                    <!-- End Contact Info -->
                </div>
                <div class="col-md-6">
                    <!-- Start Social Links -->
                    <ul class="social-list">
                        @foreach($contacts as $contact)

                            @if (is_null($contact->facebook) )
                            <li>
                            </li>
                            @else
                            <li>
                                <a class="facebook sh-tooltip" data-placement="bottom" title="Facebook" href="{{ $contact->facebook }}" target="_blank"><i class="icon-facebook-2"></i></a>
                            </li>
                            @endif

                            @if (is_null($contact->linkdin) )
                            <li>
                            </li>
                            @else
                            <li>
                                <a class="linkdin sh-tooltip" data-placement="bottom" title="Linkedin" href="{{ $contact->linkedin }}" target="_blank"><i class="icon-linkedin"></i></a>
                            </li>  
                            @endif

                            @if (is_null($contact->instgram) )
                            <li>
                            </li>
                            @else
                            <li>
                                <a class="instgram sh-tooltip" data-placement="bottom" title="Instagram" href="{{ $contact->instagram }}" target="_blank"><i class="icon-instagramm"></i></a>
                            </li>
                            @endif

                            @if (is_null($contact->youtube) )
                            <li>
                            </li>
                            @else
                            <li>
                                <a class="youtube sh-tooltip" data-placement="bottom" title="Youtube" href="{{ $contact->youtube }}" target="_blank"><i class="icon-youtube"></i></a>
                            </li>
                            @endif

                            @if (is_null($contact->twitter) )
                            <li>
                            </li>
                            @else
                            <li>
                                <a class="twitter sh-tooltip" data-placement="bottom" title="Twitter" href="{{ $contact->twitter }}" target="_blank"><i class="icon-twitter"></i></a>
                            </li>
                            @endif
                    
                        
                        @endforeach  
                    </ul>
                    <!-- End Social Links -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Top Bar -->
    
    <!-- Start Header ( Logo & Naviagtion ) -->
    <div class="navbar navbar-default navbar-top">
        <div class="container">
            <div class="navbar-header">
                <!-- Stat Toggle Nav Link For Mobiles -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="icon-menu-1"></i>
                </button>
                <!-- End Toggle Nav Link For Mobiles -->
                <a class="navbar-brand" href="{{ route('welcome') }}">
                @foreach($settings as $setting)
                    @if($setting->favicon)
                        <img alt="{{ $setting->logoalt }}" src="{{ $setting->logo }}"></a>
                    @endif
                @endforeach
                

            </div>
            <div class="navbar-collapse collapse">
                
                <!-- Start Navigation List -->
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{ route('welcome') }}">Anasayfa</a>
                    </li>
                    <li>
                        <a href="{{ route('web.pages.about-us') }}">Hakkımızda</a>
                    </li>
                    
                    <li>
                        <a href="{{ route('web.pages.referances') }}">Referanslar</a>
                    </li>
                    <li>
                        <a href="{{ route('web.pages.articles') }}">Gündem</a>							
                    </li>
                    <li><a href="{{ route('web.pages.contact') }}">İletişim</a></li>
                </ul>
                <!-- End Navigation List -->
            </div>
        </div>
    </div>
    <!-- End Header ( Logo & Naviagtion ) -->
    
</header>
<!-- End Header -->


