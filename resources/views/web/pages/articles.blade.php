@extends('layouts.outside')

@section('content')
<div class="page-banner" style="padding:100px 0; background-image: url({{ asset('web/images/referanslarimiz-wallpaper.jpg') }}); text-shadow: 1px 1px 1px white;">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h1>GÜNDEM</h1>
                <h2>Medya Reklamlarınızı Barter Formülüyle Geliştiriyoruz.</h2>
            </div>
            
        </div>
    </div>
</div>
<div id="content">
	<div class="container">
		<div class="row sidebar-page">

			<div class="col-md-9 page-content">
			
				<div class="hr1" style="margin-bottom:30px;"></div>
				
				<div class="row latest-posts-classic">
					
					
					<h4 class="classic-title"><span>Gündemimiz</span></h4>
					
				
					@foreach($articles as $article)
					<div class="col-md-6 post-row">
			


						<div class="panel-body">
						@if($article->file)
							<a href="{{ route('article', $article->slug) }}" target="blank">
								<img class="img-thumbnail image-text" style="float:left; width:180px;" alt="{{ $article->name }}" src="{{ $article->file }}"> 
							</a>
						@endif
							<div class="left-meta-post">
								<div class="post-date"><span class="day">28</span><span class="month">Dec</span></div>
							<div class="post-type"><i class="icon-picture-3"></i></div>
						</div>
							<strong class="accent-color">
								<a href="{{ route('article', $article->slug) }}" target="blank">
									{{ $article->name }}
								</a>
							</strong> 
							{{ substr($article->body,0,250)  }}
						</div>
					</div> 
					@endforeach 

					{{ $articles->render() }}
					
					
				</div>
			</div>

			
			<div class="col-md-3 sidebar right-sidebar">
				<div class="widget widget-popular-posts">
					<h4>Son Blog Yazıları <span class="head-line"></span></h4>
					<ul>
					@foreach($lastarticles as $lastarticle)
					<li>
                            <div class="widget-thumb">
                                <a  href="{{ route('article', $article->slug) }}">
                                    @if($lastarticles->file)
                                        <img  alt="{{ $lastarticles->name }}" src="{{ $lastarticles->file }}">
                                    @endif
                                </a>
                            </div>
                            <div class="widget-content">
                                <h5><a href="#">{{ $lastarticles->name }}</a></h5>
                                <span>{{ $lastarticles->created_at }}</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
					@endforeach
					</ul>
				</div>
				
				
				<!-- Tags Widget -->
				<div class="widget widget-tags">
					<h4>Etiketler <span class="head-line"></span></h4>
					<div class="tagcloud">
					@if($seoarticletags->count())
						@foreach($seoarticles as $key => $seoarticle)
							@foreach($seoarticle->tags as $tag) <a href="#">#{{$tag->name}}</a>@endforeach
						@endforeach
					@endif
					</div>
				</div>

			</div>
			<!--End sidebar-->
			
			
		</div>
	</div>
</div>
@endsection