@extends('layouts.outside')

@section('content')
<div class="page-banner" style="padding:100px 0; background-image: url({{ asset('web/images/iletisim-wallpaper.jpg') }}); text-shadow: 1px 1px 1px white;">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h1>BİZE ULAŞIN</h1>
                        <h2>Medya Barter A.Ş. ve La’Boratuar MInd Craft</h2>
					</div>
					
				</div>
			</div>
        </div>
        
<div id="content">
			<div class="container" style="padding-bottom:50px; padding-top:50px;">
            
            	<div class="row">
                
                	<div class="col-md-8">
                      
                        <!-- Classic Heading -->
                    	<h4 class="classic-title"><span>İLETİŞİM FORMU</span></h4>
                      
                        <!-- Start Contact Form -->
                    	<div id="contact-form" class="contatct-form">
                            <div class="loader"></div>
                    		<form action="{{ route('contactme.submit') }}" class="contactForm" name="cform" method="post">
                            {{ csrf_field() }}
                            	<div class="row">
                                	<div class="col-md-4">
                                    	<label for="name">İsim Soyisim / Firma Adı<span class="required">*</span></label>
                                        <span class="name-missing">Lütfen İsim yada Firma adı giriniz.</span>  
                                        <input id="name" name="name" type="text" value="" size="30">
                                    </div>
                                    <div class="col-md-4">
                                    	<label for="email">E-Posta<span class="required">*</span></label>
                                        <span class="email-missing">Lütfen geçerli bir e-posta adresi giriniz.</span> 
                                        <input id="email" name="email" type="text" value="" size="30">
                                    </div>
									
                                    <!-- <div class="col-md-4">
                                    	<label for="gsm">Telefon<span class="required">*</span></label>
                                        <span class="email-missing">Lütfen Telefon Numaranızı 5331234567 şeklinde Giriniz.</span> 
                                        <input id="gsm" name="gsm" type="text" value="" size="30">
                                    </div> -->
                                </div>
                                <div class="row">
                                	<div class="col-md-12">
                                    	<label for="message">Mesajınız</label>
                                        <span class="message-missing">Bize mesaj gönderin!</span>
                                        <textarea id="message" name="message" cols="45" rows="10"></textarea>
                                        <input type="submit" name="submit" class="button" id="submit_btn" value="Send Message">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- End Contact Form -->
                      
                    </div>
                    
                    <div class="col-md-4">
                        
                        <!-- Classic Heading -->
                    	<h4 class="classic-title"><span>İRTİBAT ADRESİMİZ</span></h4>
                        
                        <!-- Some Info -->
                        <p></p>
                        
                        <!-- Divider -->
                        <div class="hr1" style="margin-bottom:10px;"></div>
                      
                        <!-- Info - Icons List -->
                        <ul class="icons-list">
                        @foreach($contacts as $contact)
                            <li><i class="icon-location-1"></i> <strong>ADRES:</strong> {{ $contact->address }}</li>
                        @endforeach   
                        @foreach($contacts as $contact)
                            <li><i class="icon-mail-1"></i> <strong>E-POSTA:</strong> {{ $contact->email }}</li>
                        @endforeach     
                        @foreach($contacts as $contact)
                            <li><i class="icon-mobile-1"></i> <strong>TELEFON:</strong> {{ $contact->gsm }}</li>
                        @endforeach  
                        @foreach($contacts as $contact)
                            <li><i class="icon-mobile-1"></i> <strong>TELEFON:</strong> {{ $contact->gsm2 }}</li>
                        @endforeach  
                        </ul>
                      
                        <!-- Divider -->
                        <div class="hr1" style="margin-bottom:15px;"></div>
                      
                        <!-- Classic Heading -->
                        <h4 class="classic-title"><span>ÇALIŞMA SAATLERİMİZ:</span></h4>
                      
                        <!-- Info - List -->
                        <ul class="list-unstyled">
                        	<li><i class="icon icon-time"></i> <strong style="padding-right:10px;">PAZARTESİ:</strong> 08.30 - 18.00</li>
                            <li><i class="icon icon-time"></i> <strong style="padding-right:50px;">SALI:</strong> 08.30 - 18.00</li>
                            <li><i class="icon icon-time"></i> <strong style="padding-right:7px;">ÇARŞAMBA:</strong> 08.30 - 18.00</li>
							<li><i class="icon icon-time"></i> <strong style="padding-right:11px;">PERŞEMBE:</strong> 08.30 - 18.00</li>
							<li><i class="icon icon-time"></i> <strong style="padding-right:38px;">CUMA:</strong> 08.30 - 18.00</li>
							<li><i class="icon icon-time"></i> <strong  style="padding-right:5px;">CUMARTESİ:</strong> 09.30 - 14.00</li>
							<li><i class="icon icon-time"></i> <strong style="padding-right:38px;">PAZAR:</strong> KAPALI</li>
						</ul>
                      
                    </div>
                  
                </div>
         
			</div>
        </div>
        @foreach($contacts as $contact)
            <iframe src="{{ $contact->map }}" width="100%" height="550" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        @endforeach
@endsection