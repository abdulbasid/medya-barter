@extends('layouts.outside')

@section('content')
<div class="page-banner" style="padding:100px 0; background-image: url({{ asset('web/images/referanslarimiz-wallpaper.jpg') }}); text-shadow: 1px 1px 1px white;">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h1>REFERANSLAR</h1>
                <h2>Medya Reklamlarınızı Barter Formülüyle Geliştiriyoruz.</h2>
            </div>
            
        </div>
    </div>
</div>

<div id="content">
			<div class="container">
				<div class="row portfolio-page">
                    <div id="portfolio" class="portfolio-4" style="padding-top:50px;">
                    @foreach($encryprojects as $encryproject)
                        <div class="portfolio-item web drawing col-md-3">
                        	<div class="portfolio-border">
                                <div class="portfolio-thumb">
                                  <a href="{{ route('project', $encryproject->slug) }}">
                                    @if($encryproject->file)
                                        <img alt="{{ $encryproject->name }}" src="{{ $encryproject->file }}" />
                                    @endif
                                </div>
                                <div class="portfolio-details">
                                        <h4 style="text-align:center;">{{ $encryproject->name }}</h4>
                                        <span>Çalışma Modelimizi görmek için şifre giriniz</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
				</div>
			</div>
		</div>
<br/>
<br/>
<br/>
@endsection