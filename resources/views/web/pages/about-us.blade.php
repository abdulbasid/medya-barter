@extends('layouts.outside')

@section('content')
<div class="page-banner" style="padding:100px 0; background-image: url({{ asset('web/images/hakkimizda-wallpaper.jpg') }}); text-shadow: 1px 1px 1px white;">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h1>HAKKIMIZDA</h1>
                <h2>Medya Barter A.Ş. ve La’Boratuar MInd Craft</h2>
            </div>
            
        </div>
    </div>
</div>
<div id="content">
			<div class="container">
				<div class="page-content" style="padding-top:50px; padding-bottom:200px;">
                    <div class="row">
                        <div class="col-md-7">
                        @foreach($aboutuses as $aboutus)
                            <h4 class="classic-title"><span>{{ $aboutus->title }}</span></h4>
                            <p>{{ $aboutus->desciription }}</p>
                        </div>
                     
                        <div class="col-md-5"> 
                        	<div class="touch-slider" data-slider-navigation="true" data-slider-pagination="true">
                               
                                    @if($aboutus->file)
                                    <div class="item"> <img alt="{{ $aboutus->title }}" src="{{ $aboutus->file }}" /></div>
                                    <div class="item"><img alt="Laboratuar Mind Craft Logo" src="{{asset('web/images/laboratuar-hakkimizda.jpg')}}"></div>
                                    @endif
                                
                                <!-- <div class="item"><img alt="Medya Barter A.Ş. logo" src="images/medya-barter-hakkimizda.jpg"></div>
                                <div class="item"><img alt="Laboratuar Mind Craft Logo" src="images/laboratuar-hakkimizda.jpg"></div> -->
                            </div>
                        </div>
                        @endforeach
                    </div>
                  
                    <div class="col-md-12 sidebar right-sidebar">
    
                        <!-- Tags Widget -->
                        <div class="widget widget-tags">
                            <h4>Etiketler <span class="head-line"></span></h4>
                            <div class="tagcloud">

                            @if($seoabouttags->count())
                                @foreach($seoaboutes as $key => $seoabout)
                                   @foreach($seoabout->tags as $tag) <a href="#">#{{$tag->name}}</a>@endforeach
                                @endforeach
                            @endif
                            </div>
                        </div>

                    </div>
                    
                     <div class="hr5" style="margin-top:40px;margin-bottom:40px;"></div>
                  

				</div>
			</div>
		</div>


@endsection
