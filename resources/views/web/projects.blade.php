<div class="recent-projects">
    <h4 class="title"><span>Gerçekleşen Projelerimiz</span></h4>
    <div class="projects-carousel touch-carousel">
        
        @foreach($projects as $pro)
        <div class="portfolio-item item">
            <div class="portfolio-border">
                <div class="portfolio-thumb">
                        <a href="{{ route('project', $pro->slug) }}">
                        @if($pro->file)
                        <img alt="{{ $pro->name }}" src="{{ $pro->file }}" />
                        @endif
                    </a>
                </div>
            </div>
        </div>
        @endforeach

        
        
    </div>
</div>