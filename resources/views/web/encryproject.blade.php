@extends('layouts.outside')

@section('content')


@if($project->statu == 0)   
<!-- Start Page Banner -->
<div class="page-banner" style="padding:100px 0; background-image: url({{ asset('web/images/calismamodelleri-wallpaper.jpg') }}); text-shadow: 1px 1px 1px white;">
    <div class="container">
        <div class="row">
            <div class="col-md-9 " style="font-color:#ffffff;">
                <h1>Referanslarımız</h1>
                <h2>Medya Reklamlarınızı Barter Formülüyle Geliştiriyoruz.</h2>
            </div>
            
        </div>
    </div>
</div>
<!-- End Page Banner -->

<div id="content" style="padding-top:50px;">
    <div class="container">
        <div class="project-page row">
            <div class="project-media col-md-8">
                <div class="touch-slider project-slider">
                    <div class="item">
                        <a class="lightbox" title="{{ $project->name }}" href="{{ $project->file }}" data-lightbox-gallery="gallery2">
                            <div class="thumb-overlay"><i class="icon-resize-full"></i></div>
                            @if($project->file)   
                             <img alt="{{ $project->name }}" src="{{ $project->file }}">
                            @endif
                        </a>
                    </div>
                </div>
            </div>
            
            <div class="project-content col-md-4">
                <h4><span>{{ $project->name }} </span></h4>
                <p> {{ $project->desciription }}
                </p>     
            </div>
        </div>

        <div class="recent-projects row">
            <h4 class="title"><span>Gerçekleşen Projelerimiz</span></h4>
            <div class="projects-carousel touch-carousel">
            
                @foreach($projects as $pro)
                <div class="portfolio-item item">
                    <div class="portfolio-border">   
                        <div class="portfolio-thumb">
                            <a href="{{ route('project', $pro->slug) }}">
                            @if($pro->file)
                                <img alt="{{ $pro->name }}" src="{{ $pro->file }}" />
                            @endif
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>  
    </div>
</div>





@endif

        
@endsection