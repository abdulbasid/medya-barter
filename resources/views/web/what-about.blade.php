<div class="row">
                
    <div class="big-title text-center">
        <h1>Medya <span class="accent-color"> Barter</span> bir La'<span class="accent-color">Bora</span>tuar Medya kuruluşudur.</h1>
    </div>
    <div class="big-title text-center">
        <h1>Medya<span class="accent-color"> Barter</span> ne iş yapar ?</h1>
    </div>
  
    <div class="hr1" style="margin-top:20px; margin-bottom:20px;"></div>

    <div class="hr1" style="margin-top:10px; margin-bottom:10px;"></div>
    
    @foreach($whatdoesits as $whatdoesit)
    <div class="col-md-6 service-box service-icon-left">
        <div class="service-icon">
            <i class="{{ $whatdoesit->icon }} icon-mini-effect icon-effect-4 gray-icon "></i>
        </div>
        <div class="service-content">
            <h4>{{ $whatdoesit->title }}</h4>
            <p>{{ $whatdoesit->desciription }}</p>
        </div>
    </div>
    @endforeach
 
    
</div>


<div class="hr5" style="margin-top:50px; margin-bottom:55px;"></div>
      
    <div class="row">
        <div class="big-title text-center" style="padding-bottom:50px;">
            <h1>Biri <span class="accent-color">Reklam Veren</span> Diğeri <span class="accent-color">Mecra</span> </h1>
        </div>
        
        @foreach($works as $work)
        <div class="col-md-4 service-box service-center">
            <div class="service-icon">
                <i class="{{ $work->icon }} icon-large"></i>
            </div>
            <div class="service-content">
                <h4>{{ $work->title }}</h4>
                <p>{{ $work->desciription }}</p>
            </div>
        </div>
        @endforeach
        
    </div>