

<div id="slider">

    <div class="fullwidthbanner-container">
        <div class="fullwidthbanner" >
            <ul>
            @foreach($sliders as $slider)
        
          
                <li data-transition="fade" data-slotamount="7" data-masterspeed="300" >
                    <img src="{{ $slider->file }}" alt="{{ $slider->name }}" data-fullwidthcentering="on" alt="Medya Barter"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                    
                </li>

            @endforeach
            </ul>
            <div class="tp-bannertimer" style="visibility:hidden;"></div>
        </div>
    </div>

    <script type="text/javascript">
        var revapi;
        jQuery(document).ready(function() {
            revapi = jQuery('.fullwidthbanner').revolution({
                
                delay:9000,
                startwidth:1140,
                startheight:450,
                hideThumbs:200,
                
                thumbWidth:100,
                thumbHeight:50,
                thumbAmount:3,
                
                navigationType:"none",
                navigationArrows:"solo",
                navigationStyle:"round",
                
                touchenabled:"on",
                onHoverStop:"on",
                
                navigationHAlign:"center",
                navigationVAlign:"bottom",
                navigationHOffset:0,
                navigationVOffset:20,

                soloArrowLeftHalign:"left",
                soloArrowLeftValign:"center",
                soloArrowLeftHOffset:20,
                soloArrowLeftVOffset:0,

                soloArrowRightHalign:"right",
                soloArrowRightValign:"center",
                soloArrowRightHOffset:20,
                soloArrowRightVOffset:0,
                        
                shadow:0,
                fullWidth:"on",
                fullScreen:"off",
                lazyLoad:"on",

                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,

                shuffle:"off",
                
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                startWithSlide:0,	
            });
        });
    </script>
            
</div>
