 <!-- Vella JS  -->
 <script type="text/javascript" src="{{ asset('web/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/jquery.migrate.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/modernizrr.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/jquery.fitvids.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/nivo-lightbox.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/jquery.isotope.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/jquery.appear.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/count-to.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/jquery.textillate.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/jquery.lettering.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/jquery.easypiechart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/jquery.nicescroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/jquery.parallax.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/jquery.themepunch.plugins.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('web/js/script.js') }}"></script>

<!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->