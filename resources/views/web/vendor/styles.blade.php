  <!-- Bootstrap CSS  -->
  <link rel="stylesheet" href="{{ asset('web/css/bootstrap.css') }}" type="text/css" media="screen">
  
  <!-- Revolution Banner CSS -->
  <link rel="stylesheet" type="text/css" href="{{ asset('web/css/settings.css') }}" media="screen" />

  <!-- Vella CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="{{ asset('web/css/style.css') }}" media="screen">

  <!-- Responsive CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="{{ asset('web/css/responsive.css') }}" media="screen">
  
  <!-- Css3 Transitions Styles  -->
  <link rel="stylesheet" type="text/css" href="{{ asset('web/css/animate.css') }}" media="screen">

  <!-- Color CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="{{ asset('web/css/colors/red.css') }}" title="red" media="screen" />

  <!-- Fontello Icons CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="{{ asset('web/css/fontello.css') }}" media="screen">
  <!--[if IE 7]><link rel="stylesheet" href="css/fontello-ie7.css"><![endif]-->