@extends('layouts.outside')

@section('content')
<div id="content">
    <div class="container">
        <div class="row blog-post-page">
            <div class="col-md-9 blog-box">
                
                <!-- Start Single Post Area -->
                <div class="blog-post gallery-post">
                    
                    <!-- Start Single Post (Gallery Slider) -->
                    <div class="post-head">
                      
                            <div class="owl-wrapper-outer autoHeight" style="height: 350px;">
                               
                                    <div class="owl-item" style="width: 848px;">
                                        <div class="item">
                                            <a class="lightbox" title="This is an image title">
                                               
                                                @if($article->file)
                                                <img  alt="{{ $article->name }}" src="{{ $article->file }}">
                                                
                                                @endif
                                            </a>
                                        </div>
                                    </div>

    
                                   
                                
                            </div>
                        
                    </div>
                    <!-- End Single Post (Gallery) -->
                    
                    <!-- Start Single Post Content -->
                    <div class="post-content">
                        <div class="post-type"><i class=" icon-picture-1"></i></div>
                        <h2>{{ $article->name }}</h2>
                        <ul class="post-meta">
                            <li>Yazar : <a href="#">{{ $article->user->name }}</a></li>
                            <li>{{ $article->created_at }}</li>
                        </ul>
                        {{ $article->body }}</p>
                        
                        <div class="post-bottom clearfix">
          
                            <div class="post-share">
                                <span>Paylaş:</span>
                                <a class="facebook" target="_blank" href="http://www.facebook.com/sharer.php?u=http://www.facebook.com"><i class="icon-facebook"></i></a>

                                <a class="twitter" target="_blank" href="http://www.twitter.com/share?url=http://www.google.com/"><i class="icon-twitter"></i></a>
                                <a class="linkedin" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=[http://www.google.com/]&title=[test]"><i class="icon-linkedin-1"></i></a>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
     

            </div>
            
            
            
            <div class="col-md-3 sidebar right-sidebar">
    
                <div class="widget widget-popular-posts">
                    <h4>Son Blog Yazıları <span class="head-line"></span></h4>
                    <ul>
                    @foreach($lastarticles as $lastarticle)
                        <li>
                            <div class="widget-thumb">
                                <a  href="{{ route('article', $article->slug) }}">
                                    @if($lastarticles->file)
                                        <img  alt="{{ $lastarticles->name }}" src="{{ $lastarticles->file }}">
                                    @endif
                                </a>
                            </div>
                            <div class="widget-content">
                                <h5><a href="#">{{ $lastarticles->name }}</a></h5>
                                <span>{{ $lastarticles->created_at }}</span>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                        
                    @endforeach
                    </ul>
                </div>
                

                <!-- Tags Widget -->
                <div class="widget widget-tags">
                    <h4>Etiketler <span class="head-line"></span></h4>
                    <div class="tagcloud">
                    @if($seoarticletags->count())
                        @foreach($seoarticles as $key => $seoarticle)
                        @foreach($seoarticle->tags as $tag) <a href="#">#{{$tag->name}}</a>@endforeach
                        @endforeach
                    @endif
                    </div>
                </div>

            </div>
            <!--End sidebar-->
            
            
        </div>

    </div>
</div>
@endsection