<!-- Start Footer -->
<footer>
    <div class="container">
        
        
        <!-- Start Copyright -->
        <div class="copyright-section">
            <div class="row" >
                <div class="col-md-6"  >
                    <p>© 2019 Medya Barter A.Ş. bir &nbsp;&nbsp;<a href="http://www.laboratuar.com.tr" target="_blank"><img src="{{ asset('web/images/laboratuarlogo200.png')}}"></a>&nbsp;&nbsp;  Kuruluşudur.</p>
                </div>
                <div class="col-md-6">
                        <p style="width: 100%; text-align:right; ">Design&nbsp;&nbsp;<a
                        href="https://www.beyinatolyesi.com" target="_blank"><img
                            src="{{ asset('web/images/logogrey200.png')}}"  style='display: inline-block'
                            onmouseover="this.src='{{ asset('web/images/logogrey200.png')}}'"
                            onmouseout="this.src='{{ asset('web/images/logogrey200.png')}}'"
                            alt="Beyin Atölyesi" title="Beyin Atölyesi"/></a></p>
                </div>
                                        
            </div>
        </div>
        <!-- End Copyright -->
        
    </div>
    
</footer>
<!-- End Footer -->