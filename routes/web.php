<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('welcome');
});

Auth::routes();


Route::get('/welcome', 'Web\PageController@blog')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('customer')->group(function(){
    Route::get('/login', 'Auth\CustomerLoginController@showLoginForm')->name('customer.login');
    Route::post('/login', 'Auth\CustomerLoginController@login')->name('customer.login.submit');
    Route::get('/', 'CustomerController@index')->name('customer.dashboard');   
});

Route::name('web.pages.')->group(function () {
    Route::get('referances', 'Web\ReferanceController@referance')->name('referances');
    Route::get('about-us', 'Web\AboutUsController@index')->name('about-us');
    Route::get('articles', 'Web\ArticleController@index')->name('articles');
    Route::get('contact', 'Web\ContactController@index')->name('contact');
    Route::get('encryprojects', 'Web\EncryProjectController@index')->name('encryprojects');
});


Route::get('/project/{slug}', 'Web\PageController@project')->name('project');
Route::get('/slider/{slug}', 'Web\PageController@slider')->name('slider');
Route::get('/post/{slug}', 'Web\PageController@post')->name('post');
Route::get('/category/{slug}', 'Web\PageController@category')->name('category');
Route::get('/tag/{slug}', 'Web\PageController@tag')->name('tag');
Route::get('/encryproject/{slug}', 'Web\EncryProjectController@encryproject')->name('encryproject');
Route::get('/article/{slug}', 'Web\PageController@article')->name('article');


Route::resource('tags','Admin\TagController');
Route::resource('categories','Admin\CategoryController');
Route::resource('posts','Admin\PostController');
Route::resource('projects','Admin\ProjectController');
Route::resource('sliders','Admin\SliderController');
Route::resource('aboutuses','Admin\AboutusController');
Route::resource('contacts','Admin\ContactController');
Route::resource('whatdoesits','Admin\WhatDoesItController');
Route::resource('works','Admin\WorkController');
Route::resource('articless','Admin\ArticleController');
Route::resource('customers','Admin\CustomerController');
Route::resource('settings','Admin\SettingController');
Route::resource('contactmessages','Admin\ContactMessageController');

//SEO
Route::resource('homepages','Admin\HomePageController');
Route::resource('seoabouts','Admin\SeoAboutController');
Route::resource('seoreferances','Admin\SeoReferanceController');
Route::resource('seocontacts','Admin\SeoContactController');
Route::resource('seoarticles','Admin\SeoArticleController');


Route::get('article', 'ArticleController@index');
Route::post('article', 'ArticleController@store');



Route::namespace('Auth')->group(function () {
    Route::get('logout', 'LoginController@logout');
});