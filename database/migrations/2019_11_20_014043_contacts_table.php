<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address', 128);
            $table->string('email', 128);
            $table->string('gsm', 128);
            $table->string('gsm2', 128);
            $table->string('map', 512);

            $table->string('youtube', 128)->nullable();
            $table->string('linkedin', 128)->nullable();
            $table->string('instagram', 128)->nullable();
            $table->string('facebook', 128)->nullable();
            $table->string('twitter', 128)->nullable();

            $table->string('file', 128)->nullable();

            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
