<?php

use Illuminate\Database\Seeder;

class SlidersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sliders')->delete();
        
        \DB::table('sliders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'slider1',
                'slug' => 'slider1',
                'file' => 'http://localhost/medyabarter/public/image/BkzZWLcDNJRlRJ502VAHYEKqasMztDlxETF25H78.jpeg',
                'created_at' => '2019-11-19 17:43:53',
                'updated_at' => '2019-11-19 17:43:53',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'slider2',
                'slug' => 'slider2',
                'file' => 'http://localhost/medyabarter/public/image/l4yyaB1wKMUENMBWDrlKdVyicS5PQKJsiPxucNQ1.jpeg',
                'created_at' => '2019-11-19 17:44:07',
                'updated_at' => '2019-11-19 17:44:07',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'slider3',
                'slug' => 'slider3',
                'file' => 'http://localhost/medyabarter/public/image/7xpSTbgVynXOOXO5tzvDE6fWcIfZUPs7cCBkkXnt.jpeg',
                'created_at' => '2019-11-19 17:45:01',
                'updated_at' => '2019-11-19 17:45:01',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}