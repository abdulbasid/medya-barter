<?php

use Illuminate\Database\Seeder;

class WhatdoesitsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('whatdoesits')->delete();
        
        \DB::table('whatdoesits')->insert(array (
            0 => 
            array (
                'id' => 1,
                'icon' => 'icon-desktop',
                'title' => 'Mecralar',
                'desciription' => 'Anlaşmalı olduğu medya organlarının gider kalemlerini temin ederek cari alacak oluşturur. Oluşturduğu cari alacak ile edindiği reklam alanlarını reklam veren müşteriyle buluşturur. Bu sistemin tamamı barter formülüyle gerçekleştirilir.',
                'file' => NULL,
                'created_at' => '2019-11-20 03:24:26',
                'updated_at' => '2019-11-20 03:46:06',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'icon' => 'icon-users-2',
                'title' => 'Reklam Veren',
                'desciription' => 'Medya Barter’ın mecradan oluşturduğu cari alacak ile müşterilerin, reklam alanlarını barter sistemiyle kullanmasına imkan sağlar. Bu imkan ile reklam veren kullanım yüzdesini arttırır. Mecra reklamsız kuşak ve alanların doğruluğunu Medya Barter ile sağlar.',
                'file' => NULL,
                'created_at' => '2019-11-20 03:41:16',
                'updated_at' => '2019-11-20 03:45:59',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'icon' => 'icon-eye-4',
                'title' => 'Strateji',
                'desciription' => 'Medya Barter ,anlaşmalı olduğu mecranın, iletişim stratejisini oluşturmanın yanı sıra, medya planını hazırlar ve bu medya planında yer alan mecraları yine barter ile mecranın kullanıma açar.',
                'file' => NULL,
                'created_at' => '2019-11-20 03:41:42',
                'updated_at' => '2019-11-20 03:45:50',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'icon' => 'icon-pencil-4',
                'title' => 'Bütçeleme',
                'desciription' => 'Gider kalemleri ağırlığını, iletişim bütçeleri, araç kiralama hizmetleri, konaklama, seyahat ve organizasyon, promosyon başta olmak üzere event ve projelerle ilgili modelleme oluşturur.',
                'file' => NULL,
                'created_at' => '2019-11-20 03:42:05',
                'updated_at' => '2019-11-20 03:45:37',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}