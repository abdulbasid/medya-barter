<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('projects')->delete();
        
        \DB::table('projects')->insert(array (
            0 => 
            array (
                'id' => 1,
                'statu' => '0',
                'name' => 'DAMAT | TWEEN | D\'S',
                'slug' => 'damat-tween-ds',
                'desciription' => '2019’un başında radyo planlaması ve satın alma hizmeti verdiğimiz Damat ile radyo reklam kullanımının artışını sağladık.  Damat, nakit ödemede minimum harcama ile her yıl kullandığı radyo saniyesinin 4 kat fazlasını 2019’un ilk 3 çeyreğinde MedyaBarter ‘ın alışveriş kartları ile sağladı.',
                'file' => 'http://localhost/medyabarter/public/image/OTs9K9ZxfIKtygPW3VCUWNNTHVdA67xWBCNFJXdI.jpeg',
                'created_at' => '2019-11-19 16:24:50',
                'updated_at' => '2019-11-19 16:24:50',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'statu' => '0',
                'name' => 'PX',
                'slug' => 'px',
                'desciription' => 'SPX; Türkiye’ nin dev outdoor spor ürünleri satışını ve dünya markalarının distribütörlüğünü yapmaktadır.  Medya Barter ile SPX’i, CGV Mars Media Sinemaları ile buluşturduk. İlk defa barter sistemiyle sinema reklamlarına başlayan reklam verenimiz bu yıl da, Medyabarter alışveriş kartları karşılığında sinema paketi satın alarak perde reklamına devam ediyor.  SPX; sinema reklamları yanı sıra bu yıl radyo reklamlarına da bizlerle başladı',
                'file' => 'http://localhost/medyabarter/public/image/rYC9hX8DguiEecvAQnAVQHUw19wugnnua8nduNyq.jpeg',
                'created_at' => '2019-11-19 16:26:16',
                'updated_at' => '2019-11-19 16:26:16',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'statu' => '0',
                'name' => 'KİĞILI',
                'slug' => 'kigili',
                'desciription' => '2016 yılında çalışmaya başladığımız Kiğılı ile aynı yılın son çeyreğinde kullanım yaptırdığımız radyo saniyeleri, tüm reklam verenler arasında en çok kullanım rekoruna sahip. 3 aylık dönemde en fazla radyo reklamı kullanan müşteri ünvanına sahip olan Kiğılı’ya hizmet vermek bizler için de onur oldu.  Halen aynı sistem ile çalışmakta olduğumuz Kiğılı, yıl boyunca farklı dönemlerdeki radyo reklam harcamalarını Medyabarter üzerinden yapmaktadır.',
                'file' => 'http://localhost/medyabarter/public/image/qEmRcBl19UdT8S7YnWbEbLacHyUGJSpZz1O9R9M9.jpeg',
                'created_at' => '2019-11-19 16:26:39',
                'updated_at' => '2019-11-19 16:26:39',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'statu' => '0',
                'name' => 'B&G STORE',
                'slug' => 'bandg-store',
                'desciription' => 'Türkiye’nin en büyük çocuk giyim mağazalarından perakende devi B&G STORE radyo reklamlarına ilk defa bizlerle başlamıştır.  Radyo jingle söz yazımı ve bestesi tarafımızca gerçekleştirilmiş olup planlama ve satın alma yine bizim üzerimizden yapılmıştır.  Radyonun kolay ulaşılabilir ve geri dönüşlerinin etkili olmasını çok seven B&G STORE, yılın farklı zamanlarında radyo reklam kullanımlarına devam eden reklam verenimizdir.',
                'file' => 'http://localhost/medyabarter/public/image/E16ZgDPdQ6OwMXSQaQR3Uat4eyoem1M2YDV2sHrn.jpeg',
                'created_at' => '2019-11-19 16:27:03',
                'updated_at' => '2019-11-19 16:27:03',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'statu' => '0',
                'name' => 'TURA TURİZM',
                'slug' => 'tura-turizm',
                'desciription' => 'Tura Turizm ile radyo reklamlarının jingle hazırlık sürecinden planlama ve satın alma hizmetlerinin tamamı tarafımızca sağlandı.  54 yıllık güvene sahip Tura Turizm için hazırladığımız jingle çok ses getirdi.  Radyo planlamasından aldığı geri dönüşlerden memnun kalan Tura Turizm ile 3. kampanyamızı gerçekleştirdik.  Tura Turizm den elde ettiğimiz yurt dışı konaklama ve seyahat haklarımızı mecraların kullanıma açtık.',
                'file' => 'http://localhost/medyabarter/public/image/5aO4cwFjizh5b5QVcuSA9tpUuZ757CvJSv7xe2uH.jpeg',
                'created_at' => '2019-11-19 16:27:24',
                'updated_at' => '2019-11-19 16:27:24',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'statu' => '0',
                'name' => 'AVVA',
                'slug' => 'avva',
                'desciription' => 'AVVA ile 2017 yılından günümüze radyo reklam planlamasını gerçekleştirmekteyiz.  2018 de Murat Boz ile reklam çalışmalarına hız veren AVVA, radyolardan aldığı geri dönüşten memnun kalıp radyo reklam çalışmalarını arttıran reklam verenimizdir.  2019 yılının farklı periyodlarında radyo reklamlarına devam eden AVVA, 2020 planları içerisinde de Medyabarter ile ilerlemeyi hedeflemektedir.',
                'file' => 'http://localhost/medyabarter/public/image/UzmdCTQ1FZJvSYO68X3Qqaf6FJHr36q8wjFJtde6.jpeg',
                'created_at' => '2019-11-19 16:27:43',
                'updated_at' => '2019-11-19 16:27:43',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'statu' => '0',
                'name' => 'CGV MARS MEDIA',
                'slug' => 'cgv-mars-media',
                'desciription' => 'Mars Media’da son 5 yıldır, ajans müşterilerimize yüklü reklam kullanımı yaptırmaktayız.  Mars Media araç kiralama hizmetini makam araçlarında Medyabarter üzerinden almaktadır.  CGV Mars Media ile bu denli büyük hacimde barter yapan tek kuruluşuz.  2018 yılı biletli izleyici oranı 80 Milyonun üzerindedir. Kişilerin cep telefonu ile bağlarını kestikleri tek yer olan sinema salonlarında perde reklamının etkisi ve geri dönüşün gücünü reklam verenlerimizin deneyimlerindeki memnuniyetten ve Reklam Verenler Derneği tarafından yapılan araştırmada medya ve reklam yatırımlarında dijitalden sonra en çok payı alan sinema yatırımlarından ölçümlemekteyiz.',
                'file' => 'http://localhost/medyabarter/public/image/5nE9dvDt2EEcbvI4yjokFM1OMJ9zK2ua0Rca0jVZ.jpeg',
                'created_at' => '2019-11-19 16:28:59',
                'updated_at' => '2019-11-19 16:29:00',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'statu' => '0',
                'name' => 'POWER GROUP',
                'slug' => 'power-group',
                'desciription' => 'Power Group, radyolarının iletişim ajansı olarak Medyabarter’ın Grup şirketi olan La’Boratuar ’ı seçti.  2019-2020 yılları içinde tanıtım ve duyurularını ajansımız üzerinden hizmet alarak gerçekleştirecek olan Power Group, reklam saniye yayınlarını Medyabarter üzerinden müşterilerimizin kullanımına sunmaktadır.  Power Group aynı zamanda Medyabarter ‘dan araç kiralama hizmeti de almaktadır.',
                'file' => 'http://localhost/medyabarter/public/image/edJVXyzYsPyo62heWKnzB0zj1pj5RGelzpiSCfCS.jpeg',
                'created_at' => '2019-11-19 16:29:40',
                'updated_at' => '2019-11-19 16:29:40',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'statu' => '0',
                'name' => 'KARNAVAL MEDIA GROUP',
                'slug' => 'karnaval-media-group',
                'desciription' => 'Karnaval Media Group ile 2019 yılında çalışmaya başladık.  Eventlerinde, Medyabarter alışveriş kartlarını dinleyicilerine hediye ederek başlattığımız çalışma iletişim ajansı olarak hizmet vermemizin önünü açtı.  2019-2020 yılları içinde; sinema perde reklamları, açık hava, avm önü led ekranlar iletişim planlarımız arasında yer almaktadır.',
                'file' => 'http://localhost/medyabarter/public/image/SGs3QxoOstPY4V7LBJNdzXgMhnpOa9XeVRLg7S8W.jpeg',
                'created_at' => '2019-11-19 16:30:02',
                'updated_at' => '2019-11-19 16:30:02',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'statu' => '0',
                'name' => 'KAFA RADİO',
                'slug' => 'kafa-radio',
            'desciription' => 'Kafa Radyo 2019’un ilk çeyreğinde yayın hayatına başladı. Nihat Sırdar, Güçlü Mete, Candaş Tolga Işık, Sunay Akın, Zeki Kayhan Coşkun gibi birçok ünlü radyocunun buluştuğu frekans kısa sürede yüksek dinlenme oranına sahip oldu.  Kafa Radyo yeni kuruluş tanıtımını Kıbrıs’da ajansımızın organizasyonu ile gerçekleştirdi. Radyo reklam ajanslarını üç gün süreyle misafir eden Kafa Radyo yayın akışı tanıtımını gerçekleştirdiği bu organizasyon hizmetini Medyabarter üzerinden barter ile satın aldı.  Medyabarter 2019-2020’de Kafa Radyo’nun iletişim ajansı olarak; otobüs arkası (superback) reklamları ve Açıkhava reklamlarıyla hizmet vermeye devam etmektedir.',
                'file' => 'http://localhost/medyabarter/public/image/88suvbQwOCs2U6sKeFytzIT4METdqIO00BdCEXOx.jpeg',
                'created_at' => '2019-11-19 16:30:36',
                'updated_at' => '2019-11-19 16:30:36',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'statu' => '0',
                'name' => 'DEMİRÖREN YAYIN GRUBU',
                'slug' => 'demiroren-yayin-grubu',
            'desciription' => 'Demirören Grubu radyolarının 2019 ve 2020 yılı iletişim ajansı olarak Radyo D için sinema perde ve otobüs arkası (superback) reklamları, Dream ve Radyo D promosyon ürünlerin temini, Radyo D için event aracı kiralama ve kaplama yanı sıra canlı yayın aracı ekipmanları aracın kiralanması ve kaplaması tarafımızca gerçekleştirildi.  Trump ve Sapphire AVM önü Led ekranlarda 3 ay süre ile reklam görselimizin yayınlanması da devam eden çalışmalarımızdandır.',
                'file' => 'http://localhost/medyabarter/public/image/xo2oamIbihdTn6pjZqtQXi5IMkxyFYNOsIWeImFs.jpeg',
                'created_at' => '2019-11-19 16:30:59',
                'updated_at' => '2019-11-19 16:30:59',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'statu' => '0',
                'name' => 'DOĞAN YAYINCILIK',
                'slug' => 'dogan-yayincilik',
            'desciription' => 'Slow Türk ile 2019-2020 yılları arasında iletişim ajansı olarak yılın son çeyreğinde otobüs arkası (superback) reklamlarıyla çalışmalara başladık.  Trump, Sapphire AVM önü Led ekranlarda 3 ay süre ile reklam görselimizin yayınlanması da devam eden çalışmalarımızdandır.  2020 yılı iletişim stratejimizde daha güçlü bir reklam çalışması ile hacmimizi arttırmayı planlamaktayız.',
                'file' => 'http://localhost/medyabarter/public/image/CMaUkKFfmzr2YgadGWpCCC9T12ShPvku9jNtoXsI.jpeg',
                'created_at' => '2019-11-19 16:31:26',
                'updated_at' => '2019-11-19 16:31:26',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'statu' => '0',
                'name' => 'SHOW RADYO & RADYO VİVA',
                'slug' => 'show-radyo-and-radyo-viva',
                'desciription' => '2015 yılından bu yana çalıştığımız ve reklam yüzdesinin artmasına katkıda bulunduğumuz Show Radyo ve Radyo Viva ile araç kiralama hizmetinin yanı sıra, birçok gider kalemini modellemede etkili rol oynuyoruz.  Radyoların iletişim ajansı olarak sinema perde reklamlarında yıl boyunca programcı ve frekans tanıtımlarını yaparak dinleyici artışıyla ilgili çalışmalar gerçekleştiriyoruz.  2020 yılında da iletişim ajansı olarak sinema perde reklamlarının yanı sıra açık hava da reklam kullanım alanlarını arttırmayı planlamaktayız.',
                'file' => 'http://localhost/medyabarter/public/image/KyrxUjdlY4DffItblN5dJ3W7UIf36Txf5Ue9jHEa.jpeg',
                'created_at' => '2019-11-19 16:31:49',
                'updated_at' => '2019-11-19 16:31:49',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'statu' => '0',
                'name' => 'CLEAR LINE',
                'slug' => 'clear-line',
                'desciription' => 'Trump ve Sapphire cephesindeki led ekranlarında reklam satış haklarına sahip olan firma ile yıllık büyük hacimli çalışmalar gerçekleştirmekteyiz.',
                'file' => 'http://localhost/medyabarter/public/image/VV3n7D4dzML2F3zER4sOugCWistt1NzfH7FozOwQ.jpeg',
                'created_at' => '2019-11-19 16:32:10',
                'updated_at' => '2019-11-19 16:32:10',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'statu' => '0',
                'name' => 'REKLAM AREA',
                'slug' => 'reklam-area',
                'desciription' => 'Nişantaşı, Abdi İpekçi Cd. cephesindeki led ekranlarında reklam satış haklarına sahip olan firma ile yıllık büyük hacimli çalışmalar gerçekleştirmekteyiz.  Reklamarea ile Galeria, Akmerkez ve Nişantaşı led ekranlarda işbirliği içindeyiz. Yıl boyunca birçok reklam verenimiz bu mecralara ilgi göstermekte ve kullanmaktadır.',
                'file' => 'http://localhost/medyabarter/public/image/2E9TNffy7gynBqNzauNeIMoDxs2k50XB4QRYissH.jpeg',
                'created_at' => '2019-11-19 16:32:49',
                'updated_at' => '2019-11-19 16:32:50',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'statu' => '0',
                'name' => 'DOĞUŞ YAYIN GRUBU',
                'slug' => 'dogus-yayin-grubu',
                'desciription' => 'Kral FM ile 2018’in sonunda Müslüm Filminde perde reklamıyla başlayan çalışmamız, 2019 yılında iletişim ajansı olarak devam etti. Tüm iletişim planını ajansımız üzerinden hizmet alarak gerçekleştiren Doğuş Yayın Grubu Radyolarıyla, La’Boratuar 2020 yılı içinde anlaşma sağladı.

Sinema, Avrupa’da(Almanya,Avusturya,Fransa)dijital reklamlar ve You Tube, Elektrik direklerinde Pole banner, otobüs kaplama ve Modyo TV reklamları tarafımızca gerçekleştirildi.

2020 yılı iletişim stratejimizin planlaması için görüşmelerimiz devam etmektedir.',
                'file' => 'http://localhost/medyabarter/public/image/htgJG5mIBEw9tDx5Sw4gBXU5mv5yHcSXmQVFrLsp.jpeg',
                'created_at' => '2019-11-20 00:45:46',
                'updated_at' => '2019-11-20 00:45:46',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}