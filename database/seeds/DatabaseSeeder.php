<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        // $this->call(TagsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
         $this->call(ProjectsTableSeeder::class);
        $this->call(SlidersTableSeeder::class);
        $this->call(AboutusesTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(WhatdoesitsTableSeeder::class);
        $this->call(WorksTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
    }
}
