<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => '$2y$10$W3TbpgVYb3OC0BpjsZl9FOq/uHsIMD2EBkQsrUijoi/c1MfG9Z7Pu',
                'remember_token' => NULL,
                'created_at' => '2019-11-19 16:42:13',
                'updated_at' => '2019-11-19 16:42:13',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}