<?php

use Illuminate\Database\Seeder;

class AboutusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('aboutuses')->delete();
        
        \DB::table('aboutuses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Medya Barter A.Ş. ve La’Boratuar Mind Craft',
                'desciription' => 'La’Boratuar Mind Craft 2003 yılında kuruldu. Aynı yıl barter ile reklam modellemesi yapmaya başladı. Zaman içinde müşteri pörtföyü genişledi ve kalıcı hale geldi. Kurulduğu yıldan bu güne devam eden müşterileri olması ajansımızın en büyük gurur kaynağı.

Müşteri pörtfoyü gün geçtikçe büyüyen La’Boratuar; Medya Barter markasını kurarak reklam vereni, mecralarla bu şirket çatısı altında buluşturmaya başladı.

Laboratuar Mind Craft, Medya Barter A.Ş. için prodüksiyon, danışmanlık, jingle, reklam filmi, ilan tasarımları üretmeye devam etmektedir.',
                'file' => 'http://localhost/medyabarter/public/image/UyMBocu4w6udLvoqW5nrdDdew46vYKRS2hsiqBl5.jpeg',
                'created_at' => '2019-11-20 00:27:02',
                'updated_at' => '2019-11-20 00:43:39',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}