<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 6,
                'title' => 'Medya Barter A.Ş. - Sinema Reklamı, Radyo Reklamı, Televizyon Reklamında Barter Formülünü kullanın.',
                'favicon' => 'http://localhost/medyabarter/public/image/Ag71LCxNbFzTWCZbNMBkxentQhOG5WPM87ia0vlL.png',
                'logo' => 'http://localhost/medyabarter/public/image/xvxSwYDOKgjOeeN9TsnCp1ZSh3ZD8eEGnDX9kLRO.png',
                'logoalt' => 'Medya Barter',
                'created_at' => '2019-12-21 19:22:00',
                'updated_at' => '2019-12-21 19:31:27',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}