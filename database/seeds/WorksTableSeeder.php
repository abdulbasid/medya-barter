<?php

use Illuminate\Database\Seeder;

class WorksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('works')->delete();
        
        \DB::table('works')->insert(array (
            0 => 
            array (
                'id' => 1,
                'icon' => 'icon-users-2',
                'title' => 'REKLAM VEREN',
                'desciription' => 'Reklam veren: "Medya Barter" alışveriş kartlarının online satışa sunulmaması ve nakit müşteri portföyüne açılmaması garantisi.',
                'file' => NULL,
                'created_at' => '2019-11-20 14:39:24',
                'updated_at' => '2019-11-20 14:39:24',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'icon' => 'icon-desktop',
                'title' => 'MECRALAR',
                'desciription' => 'Mecra: Reklam alanlarını nakit çalıştığı müşterilerine sunulmaması garantisi.',
                'file' => NULL,
                'created_at' => '2019-11-20 14:39:53',
                'updated_at' => '2019-11-20 14:39:53',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'icon' => 'icon-smile',
                'title' => 'REFERANSLAR',
                'desciription' => 'Referanslarımız buna en güzel örnek olup, 17 yıllık barter tecrübemizle ve gerçekleştirdiğimiz projelerle bu garantiyi sağlamaktayız.',
                'file' => NULL,
                'created_at' => '2019-11-20 14:40:20',
                'updated_at' => '2019-11-20 14:40:20',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}